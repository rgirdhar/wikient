package iiit.rohit.webm;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataTypeAnalyzer {
	
	private static boolean isNumber(String inp) {
		try {
			Double.parseDouble(inp);
		} catch(NumberFormatException nfe) {
			return false;
		}
		return true;
	}
	
	private static boolean isDate(String inp, String attr_name) {
		// H1 : simply check if any name of month appears in it
		String regex = ".*\\b(january|february|march|april|june|july|august|september|october|november|december)\\b.*";
		if (inp.toLowerCase().matches(regex)) {
			//System.out.println("Matched : " + inp);
			return true;
		}
		// H2 : any integer between 1200 - 2020 
		try {
			int y = Integer.parseInt(inp);
			if (y >= 1200 && y <= 2020) {
				//System.out.println("Matched : " + inp);
				return true;
			}
		} catch (NumberFormatException nfe) {}
		// H3 : "date" appears in the attr_name
		if (inp.toLowerCase().matches(".*\\bdate\\b.*")) {
			//System.out.println("Matched : " + inp);
			return true;
		}
		return false;
	}
	
	private static boolean isSetOfStrings(String inp) {
		// H1 : strings separated by a comma
		if (inp.matches("\\w+(\\s*,\\s*\\w+)+")) return true;
		return false;
	}
	
	private static boolean isNumberWithUnits(String inp) {
		if (getUnit(inp) == null) return false;
		return true;
	}
	
	private static boolean isString(String inp) {
		if (inp.matches("\\w+.*")) return true;
		return false;
	}
	
	public static double count(ArrayList<String> l, String attr_name, DataType d) {
		double num = 0;
		int tot = Math.min(l.size(), 100);
		for (int i = 0; i < tot; i++) {
			Boolean res = false;
			if (d == DataType.DATE) res = isDate(l.get(i), attr_name);
			if (d == DataType.NUMBER) res = isNumber(l.get(i));
			if (d == DataType.SET_OF_STRINGS) res = isSetOfStrings(l.get(i));
			if (d == DataType.NUMBER_WITH_UNITS) res = isNumberWithUnits(l.get(i));
			if (d == DataType.STRING) res = isString(l.get(i));
			
			if (res) num += 1;
		}
		return num / tot;
	}
	
	public static DataType analyze(ArrayList<String> values, 
			String attr_name) {
		// try to fit a data type in the order
		double th = 0.8;		// threshold, if x% of values match the data type
		if (count(values, attr_name, DataType.NUMBER) > th) {
			return DataType.NUMBER;
		} else if (count(values, attr_name, DataType.NUMBER_WITH_UNITS) > th) {
			return DataType.NUMBER_WITH_UNITS;
		} else if (count(values, attr_name, DataType.DATE) > th) {
			return DataType.DATE;
		} else if (count(values, attr_name, DataType.SET_OF_STRINGS) > th) {
			return DataType.SET_OF_STRINGS;
		} else if (count(values, attr_name, DataType.STRING) > th) {
			return DataType.STRING;
		} else {
			return DataType.OTHER;
		}
	}
	
	public static DataType analyzeType(String value, String attr_name) {
		if (isNumber(value)) return DataType.NUMBER;
		else if (isNumberWithUnits(value)) return DataType.NUMBER_WITH_UNITS;
		else if (isDate(value, attr_name)) return DataType.DATE;
		else if (isSetOfStrings(value)) return DataType.SET_OF_STRINGS;
		else if (isString(value)) return DataType.STRING;
		else return DataType.OTHER;
	}
	
	public static String getUnit(String val) {
		Matcher m = Pattern.compile("(\\d+)\\s*(\\D{1,5})")
				.matcher(val);
		if(!m.find()) return null;
		return m.group(2).trim();
	}
}
