package iiit.rohit.webm;

public enum DataType {
	STRING,
	SET_OF_STRINGS,
	DURATION,
	SET_OF_DURATIONS,
	NUMBER,
	NUMBER_WITH_UNITS,
	DATE,
	OTHER,
}
