package iiit.rohit.webm;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.*;

public class Analyzer {
	String entType;
	Map<String, ArrayList<String>> attrs;
	
	final private String opFolder = "results/";
	final private String delim = "\t";
	
	public void analyzeAndPrint() {
		HashMap<String, AttrInfo> attrs;
		AttrInfo info;
		for (String ent_type : Parser.map.keySet()) {
			String fname = opFolder + ent_type + ".txt";
			String op = "";
			attrs = Parser.map.get(ent_type);
			attrs = tryMergers(attrs);
			for (String attr : attrs.keySet()) {
				info = attrs.get(attr);
				op += attr + delim;
				op += info.getInferredDataType() + delim;
				op += info.getUnits() + delim;
				op += info.getMinValue() + delim;
				op += info.getMaxValue() + delim;
				op += info.getMergers() + delim;
				op += info.getEntCount() + delim;
				op += "\n";
			}
			write(fname, op);
		}
	}
	
	private HashMap<String, AttrInfo> tryMergers(HashMap<String, AttrInfo> attrs) {
		boolean merged = true;
		while (merged) {
			merged = false;
			for (String s : attrs.keySet()) {
				for (String j : attrs.keySet()) {
					if (s.compareTo(j) > 0) {
						if (attrs.get(s).merge(attrs.get(j)) == 1) {
							attrs.remove(j);
							merged = true;
							break;
						}
					}
				}
				if (merged) {
					break;
				}
			}
		}
		return attrs;
	}
	
	public void write(String fname, String text) {
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(fname, "unicode");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		writer.println(text);
		writer.close();
	}
}
