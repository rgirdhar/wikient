package iiit.rohit.webm;

import java.util.*;

public class AttrInfo {
	public String attr_name;
	public Map<DataType, Integer> types;
	public Double minVal = Double.MAX_VALUE, maxVal = Double.MIN_VALUE;
	public Map<String, Character> units;
	public Integer countEnts = 0;
	private DataType inferredDType = null;
	public ArrayList<String> merged;
	final public int mergeTh = 3;
	
	AttrInfo(String attr_name) {
		this.attr_name = attr_name;
		types = new HashMap<DataType, Integer>();
		units = new HashMap<String, Character>();
		merged = new ArrayList<String>();
	}
	
	public int merge(AttrInfo other) {
		int d = LevenshteinDistance.computeLevenshteinDistance(this.attr_name, other.attr_name);
		if (other.getInferredDataType() != this.getInferredDataType()
				|| d > mergeTh) {
			return 0;				// can not be merged
		}
		// else, merge other into this
		for (DataType dt : other.types.keySet()) {
			if (this.types.containsKey(d)) {
				this.types.put(dt, this.types.get(dt) + other.types.get(dt));
 			} else {
 				this.types.put(dt, other.types.get(dt));
 			}
		}
		minVal = Math.min(this.minVal, other.minVal);
		maxVal = Math.max(this.maxVal, other.maxVal);
		this.units.putAll(other.units);
		this.countEnts += other.countEnts;
		this.merged.add(other.attr_name);
		
		return 1;
	}
	
	public void addElement(String val) {
		val = val.toLowerCase().trim();
		if (val == null || val.isEmpty()) return;
		countEnts++;
		DataType type = DataTypeAnalyzer.analyzeType(val, attr_name);
		addDType(type);
		if (type == DataType.NUMBER) {
			Double d = Double.parseDouble(val);
			maxVal = Math.max(maxVal, d);
			minVal = Math.min(minVal, d);
		} else if (type == DataType.NUMBER_WITH_UNITS) {
			String unit = DataTypeAnalyzer.getUnit(val);
			//System.out.println(">" + val + "<" + ">" + unit + "<");
			addUnit(unit);
		}
	}
	
	public String getMergers() {
		return merged.toString();
	}
	
	public Double getMinValue() {
		if (getInferredDataType() != DataType.NUMBER) {
			return -1.0;
		} else if (minVal == Double.MAX_VALUE) {
			return -1.0;
		} else {
			return minVal;
		}
	}
	
	public Double getMaxValue() {
		if (getInferredDataType() != DataType.NUMBER) {
			return -1.0;
		} else if (maxVal == Double.MIN_VALUE) {
			return -1.0;
		} else {
			return maxVal;
		}
	}
	
	public DataType getInferredDataType() {
		if (inferredDType == null) {
			inferDType();
		}
		return inferredDType;
	}
	
	public String getUnits() {
		String ans = "";
		for (String unit : units.keySet()) {
			ans += unit + ",";
		}
		if (ans.isEmpty()) return "-1";
		return ans;
	}
	
	public Integer getEntCount() {
		return countEnts;
	}
	
	private void inferDType() {
		double th = 0.8;
		if (types.containsKey(DataType.NUMBER)
				&& types.get(DataType.NUMBER) * 1.0 / countEnts >= th) {
			inferredDType = DataType.NUMBER;
		} else if (types.containsKey(DataType.NUMBER_WITH_UNITS)
				&& types.get(DataType.NUMBER_WITH_UNITS) * 1.0 / countEnts >= th) {
			inferredDType = DataType.NUMBER;
		}  else if (types.containsKey(DataType.DATE)
				&& types.get(DataType.DATE) * 1.0 / countEnts >= th) {
			inferredDType = DataType.DATE;
		} else if (types.containsKey(DataType.SET_OF_STRINGS)
				&& types.get(DataType.SET_OF_STRINGS) * 1.0 / countEnts >= th) {
			inferredDType = DataType.SET_OF_STRINGS;
		} else if (types.containsKey(DataType.STRING)
				&& types.get(DataType.STRING) * 1.0 / countEnts >= th) {
			inferredDType = DataType.STRING;
		} else {
			inferredDType = DataType.OTHER;
		}
	}
	
	private void addDType(DataType d) {
		if (types.containsKey(d)) {
			types.put(d, types.get(d) + 1);
		} else {
			types.put(d, 1);
		}
	}
	
	private void addUnit(String unit) {
		if (!units.containsKey(unit)) {
			units.put(unit, '1');
		}
	}
	
}
