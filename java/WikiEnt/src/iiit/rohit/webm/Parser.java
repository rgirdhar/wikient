package iiit.rohit.webm;

import edu.jhu.nlp.wikipedia.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.*;

public class Parser {
	
	// TODO : Make parser with persistent map
	// stores ent_type => (attr_name => (list of attr values))
	public static HashMap<String, HashMap<String, AttrInfo>> map;
	
	private static String[] splitInfoBox(String id, String ent_type) {
		Stack<Character> controls = new Stack<Character>();
		HashMap<Character, Character> inv = new HashMap<Character, Character>();
		inv.put(']', '[');
		inv.put('}', '{');
		String temp = "";
		String split_control = "90-a-094";
		for (int i = 0; i < id.length(); i++) {
			Character ch = id.charAt(i);
			if (ch == '[' ||
					ch == '{') {
				controls.push(ch);
				temp += ch;
			} else if (ch == ']'
					|| ch == '}') {
				if (controls.isEmpty() || controls.pop() != inv.get(ch)) {
					//System.err.println("Parser::splitInfoBox: Invalid control for ent type " + ent_type);
				}
				temp += ch;
			} else if (ch == '|' && controls.empty()) {
				temp += split_control;
			} else {
				temp += ch;
			}
		}
		return temp.split(split_control);
	}
	
	private static HashMap<String, String> tokenizeBoxInfo(String info_dump, String ent_type) {
		String[] parts = splitInfoBox(info_dump, ent_type);
		HashMap<String, String> hm = new HashMap<String, String>();
		for (String part : parts) {
			Matcher m = Pattern.compile("(\\w+)\\s*=\\s*(.*)")
					.matcher(part);
			if (m.find()) {
				String attr_name = m.group(1).trim();
				String attr_val = m.group(2).trim();
				if (attr_name != null && !attr_name.isEmpty()) {
					hm.put(attr_name, attr_val);
				}
			}
		}
		return hm;
	}
	
	private static void parseInfoDump(String info_dump) {
		Matcher m = Pattern.compile("Infobox\\s*([\\w\\s]+)\\s*\\|(.*)", Pattern.DOTALL)
				.matcher(info_dump);
		if (!m.find()) {
			return;
		}
		String ent_type = m.group(1).trim().toLowerCase();
		ent_type = ent_type.replaceAll("\\s", "_");
		HashMap<String, String> hm = tokenizeBoxInfo(m.group(2), ent_type);
		HashMap<String, AttrInfo> attrs;
		
		if (map.containsKey(ent_type)) {
			attrs = map.get(ent_type);
			for (String attr : hm.keySet()) {
				if (attrs.containsKey(attr)) {
					attrs.get(attr).addElement(hm.get(attr));
				} else {
					AttrInfo info = new AttrInfo(attr);
					info.addElement(hm.get(attr));
					attrs.put(attr, info);
				}
			}
			map.put(ent_type, attrs);
		} else {
			attrs = new HashMap<String, AttrInfo>();
			for (String attr : hm.keySet()) {
				AttrInfo info = new AttrInfo(attr);
				info.addElement(hm.get(attr));
				attrs.put(attr, info);
			}
			map.put(ent_type, attrs);
		}
	}
	
	public static void parse(String fname) {
		WikiXMLParser wxsp = WikiXMLParserFactory.getSAXParser(fname);
		map = new HashMap<String, HashMap<String, AttrInfo>>();
		try {
			wxsp.setPageCallback(new PageCallbackHandler() {
				int pgno = 0;
				public void process(WikiPage page) {
					pgno++;
					if (pgno % 100 == 0) {
						System.out.println("Parsed page : " + pgno);
					} 
					//else return;   // uncomment for testing, will run for every 100th page
					InfoBox ib = null;
					try {
						ib = page.getInfoBox();
					} catch (Exception e) {}
	                if (ib != null) {
	                	parseInfoDump(ib.dumpRaw());
	                }
				}
			});
			wxsp.parse();
            
            // Print for debug
			/*
            for (String key : map.keySet()) {
            	System.out.println(key);
            	for (String attr : map.get(key).keySet()) {
            		System.out.println("  " + attr + " " + map.get(key).get(attr).countEnts);
            	}
            }
            */
		} catch(Exception e) {
            e.printStackTrace();
		}
	}
}
