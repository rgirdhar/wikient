highest	STRING	-1	-1.0	-1.0	[]	26	
lon_min	NUMBER	-1	0.0	59.0	[lat_min, lon_deg]	78	
since	NUMBER	-1	1291.0	1979.0	[]	26	
highest_m	NUMBER	-1	516.0	4634.0	[]	26	
capital	OTHER	-1	-1.0	-1.0	[]	26	
districts_number	NUMBER	-1	3.0	13.0	[]	18	
largest_city	OTHER	-1	-1.0	-1.0	[]	8	
languages	OTHER	-1	-1.0	-1.0	[]	26	
municipalities_number	NUMBER	-1	3.0	388.0	[]	25	
legislative	OTHER	-1	-1.0	-1.0	[]	26	
area	NUMBER	. xxx,	37.0	7105.0	[]	27	
flag_img_path	STRING	-1	-1.0	-1.0	[coa_img_path]	4	
area_source	NUMBER	rd,th,	-1.0	-1.0	[]	4	
coa_link	STRING	-1	-1.0	-1.0	[]	1	
population_density	NUMBER	-1	26.0	26.0	[]	1	
pop_ref	NUMBER	st,rd,th,	-1.0	-1.0	[]	4	
executive_members	NUMBER	-1	5.0	7.0	[]	26	
area_rank	NUMBER	nd,st,rd,th,	-1.0	-1.0	[]	22	
german_name	STRING	-1	-1.0	-1.0	[]	8	
lowest_m	NUMBER	-1	195.0	560.0	[]	26	
abbr	STRING	-1	-1.0	-1.0	[]	26	
parliament_members	NUMBER	-1	49.0	180.0	[]	26	
locatormap_img_path	NUMBER	.,	-1.0	-1.0	[]	1	
population_rank	NUMBER	nd,st,rd,th,	-1.0	-1.0	[]	22	
local_names	STRING	-1	-1.0	-1.0	[]	26	
short_name	STRING	-1	-1.0	-1.0	[]	26	
lat_deg	NUMBER	-1	46.0	47.0	[]	26	
executive	STRING	-1	-1.0	-1.0	[]	26	
area_scale	NUMBER	-1	9.0	9.0	[]	2	
districts_designation	STRING	-1	-1.0	-1.0	[]	17	
lowest	OTHER	-1	-1.0	-1.0	[]	26	

