uninhabited_isles	OTHER	-1	-1.0	-1.0	[inhabited_isles]	0	
island_council	NUMBER	counc,	-1.0	-1.0	[]	1	
area_Hector	NUMBER	-1	41.3	41.3	[]	1	
constitutent_atolls	OTHER	-1	-1.0	-1.0	[]	0	
type	STRING	-1	-1.0	-1.0	[name]	444	
info	STRING	-1	-1.0	-1.0	[]	1	
of	NUMBER	-1	520.0	2010.0	[m]	8	
longs	NUMBER	-1	0.0	2350.0	[longm, lats]	1275	
image_size	STRING	px,	-1.0	-1.0	[]	170	
province	OTHER	-1	-1.0	-1.0	[]	1	
latin_letter	OTHER	-1	-1.0	-1.0	[]	0	
Director	STRING	-1	-1.0	-1.0	[]	1	
isles	NUMBER	(as a,	32.0	32.0	[Male]	3	
length_km	NUMBER	-1	0.15	7.8	[]	191	
letter	STRING	-1	-1.0	-1.0	[]	1	
administrative_atoll	OTHER	-1	-1.0	-1.0	[]	215	
image_caption	STRING	. res,	-1.0	-1.0	[]	23	
population_as_of	OTHER	in,	-1.0	-1.0	[]	16	
island_chief	STRING	}},	-1.0	-1.0	[]	34	
size	OTHER	-1	-1.0	-1.0	[]	0	
coordinates_type	STRING	-1	-1.0	-1.0	[]	1	
image_alt	OTHER	-1	-1.0	-1.0	[]	0	
dhivehi_letter	OTHER	-1	-1.0	-1.0	[]	0	
deputy_mayor	STRING	-1	-1.0	-1.0	[]	1	
pushpin_map_alt	OTHER	-1	-1.0	-1.0	[]	0	
area_km2	NUMBER	-1	0.044	1.673	[]	13	
pushpin_label_position	STRING	-1	-1.0	-1.0	[]	219	
island_councilor	STRING	-1	-1.0	-1.0	[island_Counselor]	14	
area	NUMBER	-1	0.269	30.6	[]	6	
longEW	STRING	-1	-1.0	-1.0	[]	222	
geographic_atoll	OTHER	°,	-1.0	-1.0	[]	68	
map	OTHER	-1	-1.0	-1.0	[alt]	0	
distance_km	NUMBER	-1	2.0	539.0	[distance]	210	
coordinates	NUMBER	|,	-1.0	-1.0	[]	1	
population	NUMBER	(as a,(as o,(,,,	0.0	11140.0	[]	219	
latNS	STRING	-1	-1.0	-1.0	[]	221	
population_footnotes	OTHER	&atol,	-1.0	-1.0	[]	4	
image_skyline	STRING	,-e-,.jpg,	-1.0	-1.0	[]	33	
pushpin_map	STRING	-1	-1.0	-1.0	[]	219	
councilor	OTHER	-1	-1.0	-1.0	[Council]	1	
mayor	STRING	-1	-1.0	-1.0	[]	1	
width_km	NUMBER	-1	0.05	1625.0	[width]	207	
caption	OTHER	-1	-1.0	-1.0	[]	0	
atoll_councilor	OTHER	-1	-1.0	-1.0	[]	0	
pushpin_map_caption	STRING	-1	-1.0	-1.0	[]	219	
atolls	OTHER	-1	-1.0	-1.0	[]	1	
pushpin_mapsize	NUMBER	-1	250.0	250.0	[]	218	
native_name	OTHER	-1	-1.0	-1.0	[]	21	
President	STRING	-1	-1.0	-1.0	[]	2	
total_isles	OTHER	-1	-1.0	-1.0	[]	0	

