unemployment	NUMBER	% ({{,est.),% inc,% off,%,|alt=,% (au,% ([[,.,,,% (ma,-,%  (e,–,% (fy,% (br,% (,% in,	-1.0	-1.0	[]	183	
deficit	NUMBER	billi,% of,.,	-1.0	-1.0	[Deficit]	15	
expenses	NUMBER	billi,milli,.,,,	-1.0	-1.0	[]	179	
organs	OTHER	rd]]),major,	-1.0	-1.0	[]	162	
year	OTHER	–,– sep,janua,march,april,– jun,octob,apr -,july,to ma,	-1.0	-1.0	[]	175	
salary	NUMBER	bam,bgn (,month,ltl /,€ /,€ (av,,,/mont,lvl /,	-1.0	-1.0	[]	35	
credit	OTHER	d,),.,outlo,	-1.0	-1.0	[]	114	
currency_code	STRING	-1	-1.0	-1.0	[]	1	
exchange	NUMBER	[[uzb,[[rin,[[bru,[[kyr,falkl,.,=,tenge,	-1.0	-1.0	[]	10	
occupations	NUMBER	% (mo,%) se,%; ag,%)|se,% (,%, se,}}),% in,%) ag,%, [[,%; '',% of,%), (,%, ma,%), i,%,%)({{,%), w,.,,,%; in,% (su,%), [,%, in,%), c,	-1.0	-1.0	[]	169	
work	OTHER	-1	-1.0	-1.0	[]	0	
index	NUMBER	billi,.,	-1.0	-1.0	[Forex]	2	
width	OTHER	px,p,	-1.0	-1.0	[]	125	
agriculture	STRING	-1	-1.0	-1.0	[]	1	
economy	NUMBER	.,	-1.0	-1.0	[]	1	
spelling	OTHER	-1	-1.0	-1.0	[]	139	
image	OTHER	,janua,yer o,jod o,.jpg,cent.,-,).jpg,nakfa,n.jpg,at ni,-nati,vb.jp,f-ban,-pula,night,th oc,-rev.,pound,x,	-1.0	-1.0	[date]	131	
sectors	NUMBER	%), o,%), m,%; mi,%; [[,%), i,.,%); i,,,–,%; in,%; se,%), [,%}},,%, in,) ser,	-1.0	-1.0	[]	182	
country	STRING	-1	-1.0	-1.0	[]	198	
shares	NUMBER	%:,.,	-1.0	-1.0	[share, rates]	3	
imagel	STRING	-1	-1.0	-1.0	[]	1	
tax	NUMBER	nd (,st]],&nbsp,bn ({,th]],rd]],milli,({{as,th ou,rd,th (p,th,bn (,nd [[,th (e,nd ]],% (,th (i,st,mn (,nd]],th ([,|alt=,(,),.,,,billi,st (i,th (,nd,st (n,	10.0	208.0	[GPD, rank, gdp, PPP, rate]	570	
components	OTHER	.,	-1.0	-1.0	[]	4	
inflation	NUMBER	% ({{,% (,%,.,,,	-1.0	-1.0	[]	185	
goods	OTHER	%, ma,%, ca,%, ra,%, mi,%, fo,),%), c,.,	-1.0	-1.0	[]	175	
accessdate	NUMBER	-,	-1.0	-1.0	[]	1	
investment	NUMBER	.,	-1.0	-1.0	[]	1	
currency	OTHER	[[mac,[[pou,|xaf],[[dan,[[nor,[[geo,[[bul,[[dom,[[eur,euro,px]]),[[ind,[[man,[[gua,[[mal,[[nep,[[ira,[[bel,[[ukr,[[cen,janua,[[lat,[[mon,[[new,cordo,[[eri,.,[[swi,[[pak,[[som,centa,salva,[[uni,[[arm,[[zlo,	-1.0	-1.0	[]	193	
publisher	STRING	-1	-1.0	-1.0	[]	1	
news_id	NUMBER	bvi g,	-1.0	-1.0	[]	1	
partners	NUMBER	%,  {,%, de,%,tai,),.,%, ch,%, [[,	-1.0	-1.0	[]	182	
poverty	NUMBER	%  (,% of,% ({{,%,|alt=,% wit,% ear,),.,perso,% usi,% (fy,% (,% (be,	-1.0	-1.0	[]	157	
internal	NUMBER	.,	-1.0	-1.0	[external]	2	
population	NUMBER	.,,,	-1.0	-1.0	[]	2	
revenue	NUMBER	billi,milli,.,,,	-1.0	-1.0	[]	179	
cianame	STRING	oct,	-1.0	-1.0	[]	167	
organizations	OTHER	-1	-1.0	-1.0	[]	1	
dollars	NUMBER	,,	-1.0	-1.0	[]	1	
caption	OTHER	cent,.jpg|,malot,s,s for,%, ea,jorda,yemen,.,euro,syria,	-1.0	-1.0	[]	119	
commodities	STRING	-1	-1.0	-1.0	[]	1	
domestic	NUMBER	.,	-1.0	-1.0	[]	1	
imports	NUMBER	bn (,billi,milli,.,,,	-1.0	-1.0	[exports]	373	
labour	NUMBER	|alt=,milli,.,,,	-1.0	-1.0	[labor]	182	
reserves	NUMBER	billi,milli,.,,,	-1.0	-1.0	[Reserves]	99	
rank2	NUMBER	-1	118.0	118.0	[]	1	
industries	OTHER	est.),%), f,),).,	-1.0	-1.0	[]	191	
capita	NUMBER	ppp (,,(west,([[pu,(,.,({{as,(nomi,,,	-1.0	-1.0	[]	189	
edbr	NUMBER	thst,% of,st]],&nbsp,|alt=,milli,(,.,rd,,,th,billi,nd  (,nd,th (,}}),st,th of,	0.0	153.0	[debt]	325	
Remittance	NUMBER	.,	-1.0	-1.0	[]	1	
force	NUMBER	.,	-1.0	-1.0	[]	1	
growth	NUMBER	% ({{,% — -,% (,% ([[,.,,,	-1.0	-1.0	[]	188	

