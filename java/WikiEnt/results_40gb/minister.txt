office9	OTHER	–,nd [[,/the,)|lab,rd [[,of my,px]],st [[,nd el,th [[,-,	-1.0	-1.0	[office8, office5, office4, office7, office6, office1, office3, office2, office]	560	
profession	OTHER	-1	-1.0	-1.0	[]	85	
religion	OTHER	}},	-1.0	-1.0	[relations]	186	
spouse	OTHER	–,april,, div,-pres,),-,	-1.0	-1.0	[]	134	
movement	OTHER	-1	-1.0	-1.0	[]	0	
honorific_prefix	STRING	-1	-1.0	-1.0	[]	1	
signature	OTHER	-1	-1.0	-1.0	[]	43	
peerage	OTHER	-1	-1.0	-1.0	[]	0	
governor_general	OTHER	-1	-1.0	-1.0	[]	1	
nickname	OTHER	-1	-1.0	-1.0	[]	1	
portfolio	NUMBER	-,	-1.0	-1.0	[]	1	
image	OTHER	,.png,-d-,.jpg,bild,th du,-dscf,july,-crop,fp,edmun,c,b.jpg,(,bild-,.,-n-,,,-,oblje,(crop,theo,).jpg,detai,summe,cropp,-v,	-1.0	-1.0	[State]	249	
data5	OTHER	[[,	-1.0	-1.0	[data4]	6	
Facebook	OTHER	-1	-1.0	-1.0	[]	2	
box_width	OTHER	-1	-1.0	-1.0	[]	1	
restingplace	OTHER	-1	-1.0	-1.0	[resting_place]	4	
term7	OTHER	may,	-1.0	-1.0	[]	2	
term6	NUMBER	(govt,	1962.0	1973.0	[term5]	3	
languagesspoken	OTHER	-1	-1.0	-1.0	[]	1	
ambassador_from5	STRING	-1	-1.0	-1.0	[ambassador_from4]	8	
country5	STRING	-1	-1.0	-1.0	[country4]	9	
leader5	OTHER	-1	-1.0	-1.0	[leader3]	12	
minister4	OTHER	-1	-1.0	-1.0	[minister3]	15	
years_active	OTHER	-1	-1.0	-1.0	[]	0	
death_place	OTHER	–,	-1.0	-1.0	[birth_place]	494	
majority8	NUMBER	,,	-1.0	-1.0	[majority4, majority5, majority6]	34	
dead	STRING	-1	-1.0	-1.0	[]	2	
death_date	NUMBER	janua,may,april,th ap,june,}},or,decem,st no,,,march,augus,|df=y,octob,sept,may,,july,|,febru,novem,	1642.0	2007.0	[birth_date]	548	
width	NUMBER	px,	-1.0	-1.0	[]	1	
url	OTHER	-1	-1.0	-1.0	[end]	1	
footnotes	OTHER	octob,in fe,	-1.0	-1.0	[]	132	
death_cause	OTHER	-1	-1.0	-1.0	[]	0	
party3	OTHER	) [[c,) [[d,}},allia,px]],),+]],,,)  [[,-,–,march,/die,)|ger,	-1.0	-1.0	[party2]	371	
predecessor8	OTHER	nd du,st vi,st ba,,,	-1.0	-1.0	[predecessor6, predecessor7]	607	
battles	OTHER	-1	-1.0	-1.0	[]	2	
deputy5	OTHER	-1	-1.0	-1.0	[deputy4]	8	
Twitter	OTHER	-1	-1.0	-1.0	[]	1	
deputy6	STRING	-1	-1.0	-1.0	[]	1	
weight	OTHER	-1	-1.0	-1.0	[height]	0	
employer	STRING	-1	-1.0	-1.0	[]	1	
children	OTHER	–,)and,), yu,) and,sons,sons,,),,,daugh,	-1.0	-1.0	[]	80	
education	OTHER	-1	-1.0	-1.0	[]	2	
blank1	STRING	-1	-1.0	-1.0	[]	5	
organization	STRING	-1	-1.0	-1.0	[]	1	
pronunciation	OTHER	-1	-1.0	-1.0	[]	0	
name	STRING	.,	-1.0	-1.0	[alt]	490	
signature_size	OTHER	-1	-1.0	-1.0	[]	0	
cabinet	OTHER	th go,-,	-1.0	-1.0	[]	5	
imagesize	NUMBER	px,	154.0	260.0	[image_size]	90	
constituency_MP6	OTHER	–,[[buc,st ti,nd ti,-,	-1.0	-1.0	[constituency_MP5, constituency6]	49	
general	OTHER	-1	-1.0	-1.0	[]	0	
order8	NUMBER	th,	-1.0	-1.0	[]	1	
unit	NUMBER	st co,	-1.0	-1.0	[]	1	
native_name_lang	STRING	-1	-1.0	-1.0	[]	27	
order7	OTHER	–,of my,, yan,nd,st [[,st,th [[,th,	-1.0	-1.0	[order6, order1, order5, order4, order3, order2, order]	286	
nationality	OTHER	px]],s–,	-1.0	-1.0	[]	289	
native_name	OTHER	-1	-1.0	-1.0	[]	29	
monarch6	OTHER	–,	-1.0	-1.0	[monarch, monarch2, monarch3, monarch1]	18	
military_data5	OTHER	-1	-1.0	-1.0	[military_data4, military_data1, military_data3, military_data2]	10	
demise_place	OTHER	-1	-1.0	-1.0	[]	0	
preceded7	OTHER	-1	-1.0	-1.0	[preceded6]	2	
succeeded7	OTHER	-1	-1.0	-1.0	[succeeded6]	2	
influences	OTHER	-1	-1.0	-1.0	[influenced]	0	
rank	OTHER	nd li,	-1.0	-1.0	[blank5, branch]	63	
almamater	OTHER	[[kin,dagon,[[sta,),), [[,; [[a,-,)  [[,	-1.0	-1.0	[alma_mater]	223	
Born	NUMBER	-,	-1.0	-1.0	[]	1	
viceprimeminister6	OTHER	-1	-1.0	-1.0	[viceprimeminister4]	0	
parliament6	STRING	-1	-1.0	-1.0	[parliament4, parliament5]	19	
module6	OTHER	-1	-1.0	-1.0	[module3, module4, module5]	0	
military_blank5	OTHER	-1	-1.0	-1.0	[military_blank4, military_blank3, military_blank2, military_blank1]	0	
constituency8	OTHER	([[bu,– war,	-1.0	-1.0	[constituency7]	36	
Secretary	OTHER	-1	-1.0	-1.0	[]	1	
viceminister2	STRING	-1	-1.0	-1.0	[]	1	
allegiance	OTHER	-1	-1.0	-1.0	[]	25	
smallimage	STRING	-1	-1.0	-1.0	[]	2	
opponents	OTHER	-1	-1.0	-1.0	[]	0	
term_end5	OTHER	janua,may,april,june,}},decem,,,-,march,augus,septe,nov,octob,sept,july,|,febru,novem,	-1.0	-1.0	[term_end3]	579	
caption	OTHER	, [[d,may,april,natio,june,'',confe,),.,th af,,,- jul,-,march,days,s,in [[,summe,[[nhs,novem,	-1.0	-1.0	[]	100	
otherparty	OTHER	-,	-1.0	-1.0	[]	14	
primeminister7	OTHER	–,&ndas,) {{-,) [[j,) [[h,)  [[,-,	-1.0	-1.0	[primeminister6]	315	
boards	OTHER	-1	-1.0	-1.0	[awards]	27	
term_start6	OTHER	janua,may,april,}},june,jul,decem,,,th,june),-,augus,march,septe,nov,octob,july,|,aug,febru,novem,	-1.0	-1.0	[term_start5]	823	
residence	OTHER	–,parga,	-1.0	-1.0	[president8]	182	
notable_works	OTHER	-1	-1.0	-1.0	[]	0	
term_end9	NUMBER	may,janua,april,}},june,decem,,,march,augus,octob,july,|,febru,	1921.0	2009.0	[term_end7, term_end8]	79	
term_start9	NUMBER	may,janua,augus,april,june,septe,octob,decem,july,|,febru,,,	1911.0	2000.0	[term_start1, term_start7, term_start8]	48	
ethnicity	OTHER	-1	-1.0	-1.0	[]	12	
1blankname3	STRING	-1	-1.0	-1.0	[1blankname]	2	
succeeding	STRING	-1	-1.0	-1.0	[]	1	
serviceyears	OTHER	–,	-1.0	-1.0	[]	16	
lieutenant6	OTHER	-1	-1.0	-1.0	[lieutenant2]	0	
known_for	OTHER	-1	-1.0	-1.0	[]	0	
alongside6	OTHER	-1	-1.0	-1.0	[alongside3]	9	
taoiseach2	OTHER	-1	-1.0	-1.0	[taoiseach]	0	
other_names	STRING	-1	-1.0	-1.0	[]	1	
denomination	OTHER	-1	-1.0	-1.0	[]	0	
constituency_AM5	STRING	-1	-1.0	-1.0	[]	1	
occupation	OTHER	&ndas,	-1.0	-1.0	[]	53	
1namedata3	OTHER	-1	-1.0	-1.0	[1namedata]	2	
mother	OTHER	-1	-1.0	-1.0	[father]	0	
vicepresident6	OTHER	-1	-1.0	-1.0	[vicepresident2, vicepresident3, vicepresident4]	0	
relatives	OTHER	-1	-1.0	-1.0	[]	0	
birthname	STRING	-1	-1.0	-1.0	[birth_name]	16	
signature_alt	OTHER	-1	-1.0	-1.0	[]	2	
commands	NUMBER	th si,nd ba,-,	-1.0	-1.0	[]	5	
committees	NUMBER	),	-1.0	-1.0	[]	1	
citizenship	OTHER	-1	-1.0	-1.0	[]	3	
succeeding6	OTHER	-1	-1.0	-1.0	[succeeding2]	0	
restingplacecoordinates	OTHER	-1	-1.0	-1.0	[]	0	
governor6	OTHER	-1	-1.0	-1.0	[governor2]	1	
title4	OTHER	-1	-1.0	-1.0	[title, title3, title2]	23	
website	OTHER	}},knut,	-1.0	-1.0	[]	65	
premier2	OTHER	-1	-1.0	-1.0	[premier]	3	
premier3	NUMBER	–)  [,	-1.0	-1.0	[]	1	
suffix	OTHER	-1	-1.0	-1.0	[prefix]	105	
chancellor3	OTHER	-1	-1.0	-1.0	[chancellor2]	8	
assembly5	STRING	-1	-1.0	-1.0	[assembly3]	2	
sucessor3	OTHER	st vi,rd ma,,,th ea,-,	-1.0	-1.0	[successor8]	491	

