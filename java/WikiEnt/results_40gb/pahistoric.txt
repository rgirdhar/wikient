visitation_year	OTHER	-1	-1.0	-1.0	[]	0	
location	OTHER	,heinz,pennr,clyde,arch,fisk,ridge,pius,cruci,jones,fourt,[[bec,grand,sixth,th av,, dur,devon,/,and i,,,sycam,mayfl,-,wabas,, sit,boyle,apple,liver,/ old,bingh,beech,west,centr,north,veron,child,ohio,th st,east,fifth,monon,lowen,(wyom,green,smith,[[fif,[[bou,crawf,herro,old t,south,brown,elgin,hamil,first,	-1.0	-1.0	[caption]	113	
locmapin	STRING	-1	-1.0	-1.0	[]	95	
PAhistoric_type3	STRING	-1	-1.0	-1.0	[PAhistoric_type2]	118	
architecture	OTHER	-1	-1.0	-1.0	[architect]	35	
designated_PAhistoric_CD	OTHER	-1	-1.0	-1.0	[]	0	
image_size	NUMBER	-1	325.0	375.0	[]	3	
longitude	NUMBER	-1	-80.034167	41.353783	[latitude]	50	
designated_other3_number	OTHER	-1	-1.0	-1.0	[designated_other2_number, designated_other1_number, designated_other3_name]	0	
designated_PAhistoric_CP	NUMBER	,,	-1.0	-1.0	[]	45	
nickname	OTHER	-1	-1.0	-1.0	[]	0	
map_caption	STRING	old t,in pi,centr,ohio,north,and n,mayfl,-,	-1.0	-1.0	[]	89	
designated_other3_textcolor	OTHER	-1	-1.0	-1.0	[designated_other2_textcolor, designated_other1_textcolor]	0	
image_caption	OTHER	-1	-1.0	-1.0	[]	0	
governing_body	OTHER	-1	-1.0	-1.0	[]	30	
embed	STRING	-1	-1.0	-1.0	[]	1	
nearest_city	OTHER	-1	-1.0	-1.0	[]	5	
designated_other3_num_position	OTHER	-1	-1.0	-1.0	[designated_other2_num_position]	0	
map_width	NUMBER	-1	235.0	235.0	[]	4	
designated_other3_short	OTHER	-1	-1.0	-1.0	[designated_other2_short, designated_other1_short]	0	
district_map	OTHER	-1	-1.0	-1.0	[]	0	
designated_other1_abbr	OTHER	-1	-1.0	-1.0	[]	1	
designated_other3_link	OTHER	-1	-1.0	-1.0	[designated_other2_link]	0	
area	NUMBER	|acre,|sqft,.,,,	-1.0	-1.0	[]	4	
name	STRING	old t,d,.png,centr,oldth,).jpg,.jpg,s.jpg,th st,north,and n,mayfl,	-1.0	-1.0	[image]	186	
built	OTHER	to,, rem,- wab,or,s (?),,,(with,and,-,–,s,compa,;,(chur,th st,?,	-1.0	-1.0	[alt]	94	
long_seconds	NUMBER	-1	0.6	59.91	[lat_seconds]	142	
coordinates	OTHER	-1	-1.0	-1.0	[]	0	
designated_other3_date	OTHER	-1	-1.0	-1.0	[designated_other2_date, designated_other2_name]	1	
visitation_num	OTHER	-1	-1.0	-1.0	[]	0	
designated_PAhistoric_PHLF	NUMBER	and,	1968.0	2009.0	[]	55	
designated_other1_color	NUMBER	dc,	-1.0	-1.0	[]	1	
long_minutes	NUMBER	-1	0.0	59.0	[lat_minutes]	142	
coord_parameters	STRING	-1	-1.0	-1.0	[]	20	
coord_format	STRING	-1	-1.0	-1.0	[]	4	
long_direction	STRING	-1	-1.0	-1.0	[lat_direction]	142	
locator_y	NUMBER	-1	63.0	165.0	[locator_x]	2	
designated_other3_color	OTHER	-1	-1.0	-1.0	[designated_other2_color]	0	
coord_display	SET_OF_STRINGS	-1	-1.0	-1.0	[]	96	
long_degrees	NUMBER	-1	40.0	80.0	[lat_degrees]	142	
demolished	STRING	-1	-1.0	-1.0	[]	1	
map_alt	STRING	-1	-1.0	-1.0	[]	2	
designated_PAhistoric_type	NUMBER	,,	-1.0	-1.0	[]	17	

