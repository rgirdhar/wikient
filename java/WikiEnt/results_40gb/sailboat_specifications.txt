location	OTHER	racin,, san,ff fo,foot,being,s sta,dingh,saili,"mr b,'the,ancho,s dow,at th,under,s rac,mark,.,s s&s,, nar,' ski,at le,world,downw,s und,	-1.0	-1.0	[caption]	101	
mainsail	NUMBER	m^,ft{{s,|m,sq.ft,ft,|sqft,.,|sqm|,|ft,sq ft,	-1.0	-1.0	[]	139	
ballast	OTHER	-1	-1.0	-1.0	[]	0	
type	OTHER	|kg|a,	-1.0	-1.0	[]	117	
updated	OTHER	-sep-,-aug-,-nov-,janua,augus,march,septe,octob,-apr-,aug,febru,,,	-1.0	-1.0	[]	83	
role	STRING	' din,ff,.,	-1.0	-1.0	[name, Name]	132	
year	NUMBER	",|m|ab,mm,|ft|m,|in|m,',|mm|f,m,ft {{,.,|m|ft,|mm|a,-,|ft|,–,cm,|ft|a,ft,ft (,(offi,	25.26	2011.0	[loa, area, len, beam]	551	
class_imagesize	NUMBER	px,	41.0	200.0	[]	40	
mainsailandjib	NUMBER	m^,ft{{s,|m,ft,|sqft,.,|sqm|,m{{su,|ft,|-|,	-1.0	-1.0	[]	83	
image	OTHER	racin,,.png,foot,s.jpg,.jpg,er go,dingh,arrie,m_,drawi,saili,_,sailb,_logo,wiki,leigh,tasma,f,.jpg|,er sk,off n,boats,s_rac,m,px| ],tasar,.,-,.svg,-pico,gs,er.jp,_thau,	-1.0	-1.0	[]	139	
number	NUMBER	-1	91.9	91.9	[]	1	
class_image	OTHER	.png,er bl,.jpg,class,logo.,sail,.,black,	-1.0	-1.0	[]	48	
country	OTHER	-1	-1.0	-1.0	[]	0	
sailarea	NUMBER	ft{{s,|sqft,.,|ft,	-1.0	-1.0	[]	26	
numberofcrew	STRING	-1	-1.0	-1.0	[]	1	
pn	NUMBER	|m|ab,;{{co,|ft|m,',|mm|f,(,''(te,m,.,''(fe,,,|ft,|ft|,ft.,m^,(in,(mk i,|m,ft{{s,''(rs,ft,|sqft,(comp,	-9.0	1646.0	[loh, phrf, lwl]	284	
spinnaker	NUMBER	m^,ft{{s,|m,sq.ft,ft,|sqft,.,m{{su,m² (,sq ft,|ft,|-|,	-1.0	-1.0	[Spinnaker, gennaker]	117	
Ballast	NUMBER	.,	-1.0	-1.0	[]	1	
class_imagealt	STRING	-1	-1.0	-1.0	[]	15	
olympic	OTHER	onwar,	-1.0	-1.0	[]	215	
weight	NUMBER	|lb|k,	-1.0	-1.0	[]	1	
trapeze	STRING	-1	-1.0	-1.0	[]	23	
draught	NUMBER	|m|ab,mm,|ft|m,|in|m,m (,',|mm|f,.,|m|ft,|mm|a,|ft|,ft.,|to|,|in|a,|in|c,ft,|m|,	-1.0	-1.0	[draft]	117	
hull	NUMBER	|lb|k,kg (,|kg|a,&nbsp,|lbs|,.,,,kg (~,kg,|lb|a,|kg|l,lbs (,kg (w,lb,lbs,	-1.0	-1.0	[]	212	
class_symbol	OTHER	metre,erxx,.jpg|,saili,px]],logo.,.,dingh,saile,x,	-1.0	-1.0	[]	57	
mastheight	NUMBER	|ft|,mm,|ft|m,|cm|a,|ft|a,feet,|mm|f,m,.,|mm|a,	-1.0	-1.0	[]	69	
displacement	NUMBER	|kg|a,|kg|l,	-1.0	-1.0	[]	2	
Builder	STRING	-1	-1.0	-1.0	[]	1	
capacity	NUMBER	l,	-1.0	-1.0	[]	1	
construction	OTHER	layer,/,	-1.0	-1.0	[]	75	
website	NUMBER	.org,	-1.0	-1.0	[]	1	
Volume	NUMBER	|l|ab,	-1.0	-1.0	[]	1	
designer	OTHER	px]],desig,	-1.0	-1.0	[design]	154	
imagesize	NUMBER	px,	157.0	300.0	[]	64	
rig	OTHER	to,|kg|a,(max.,(skip,(doub,–,(sing,heeli,(max),prepa,or,(twin,at th,(trip,+,(,s lea,(with,([[tr,), on,-,peopl,(curr,|kg|l,(max,part),skipp,'''pa,	-1.0	-1.0	[crew, alt]	307	
playername	STRING	-1	-1.0	-1.0	[]	1	
keel	OTHER	|kg|a,feet,.,	-1.0	-1.0	[]	66	
ISAF	OTHER	-1	-1.0	-1.0	[]	1	

