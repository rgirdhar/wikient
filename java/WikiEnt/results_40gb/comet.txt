discoverer	OTHER	) and,.,-g,-,	-1.0	-1.0	[]	181	
orbital_circ	NUMBER	.,	-1.0	-1.0	[]	1	
last_p	NUMBER	janua,april,decem,-apr-,-mar-,,,-,apr.,augus,march,dec,-may-,dec.,septe,-jan-,-jun-,octob,july,	2009.0	2009.0	[]	178	
designations	NUMBER	cb,xi,vi;,ex|,bv,v,,iv;,tn;,, gre,xiv;,i, p/,xiii,nl|,iv, p,xxxvi,xi;,b; bu,i;,iii,,xvi;,xi,,,,x;,comet,viii;,vi,,iii;,;,, c/,vi,jd;,ix;,ud,,xix;,py|,ii;,f,d,xxvii,e,b,; gre,c,n,o,l,ud|,m,h,i,w,v;,v,}}, {,u,p, ma,t,i, gr,s,xiii;,r,q,p,xxxi;,y,x,vii;,p/,	1652.0	1652.0	[]	151	
epoch	NUMBER	-aug-,-sep-,-dec-,june,-feb-,sept.,-apr-,.,-mar-,,,-,-nov-,-oct-,dec,-jul-,-may-,feb,-jan-,-jun-,july,	2382760.5	2453754.5	[]	168	
abs_magnitude	NUMBER	-1	6.3	6.3	[]	1	
caption	NUMBER	decem,d/bro,over,, soo,st,,septe,janua,e,b,a,o,p/mac,(,l,.,/,,," [[r,s,p whe,q,st (t,, as,fragm,x,	-1.0	-1.0	[]	28	
inclination	NUMBER	°,.,,,	59.366	134.7933	[]	182	
period	NUMBER	thous,[[jul,}},&nbsp,milli,±,.,,,? [[j,	5.29	18.45	[]	173	
eccentricity	NUMBER	(assu,.,	0.02799	1.0052	[]	180	
perihelion	NUMBER	.,au,	0.405011	4.24985	[]	182	
discovery_date	NUMBER	may,june,decem,,,and,-,-nov-,march,augus,-oct-,-jul-,septe,octob,july,febru,novem,	1786.0	2004.0	[]	186	
source	OTHER	f,l,	-1.0	-1.0	[]	4	
name	NUMBER	,.jpg],p/sla,p/ote,p/hon,p/hol,p/cro,p/olb,vz|,p/väi,p/swi,d/wes,on,pneat,p/for,og,p/how,p/boe,p/tak,p/kle,.jpg|,p/cla,p/mue,p che,-,p/ike,no|,p kop,p/kus,p/gri,mcnau,p/wol,p/rus,p/yeu,px|co,p/kop,p/dan,p/bar,p/chr,p/kow,_o,p/che,f,g,d,e,b,a,n,p/fay,o,p/gia,p/fin,l,p/gic,m,j,k,h,p.htm,p/ste,w,v,u,t,p_har,s,p/har,r,q,p/cin,p,p-,y,p sch,x,dbror,p/spa,tg,p/sho,p/are,d/bro,p/spi,p/tem,p/bus,p/lin,p/whi,p/com,p/mcn,p/de,p/sin,px| c,d/per,comet,p/du,d/bie,p gia,p/tut,p/par,d/neu,p/wil,p/wis,p/joh,p/wir,p/gun,p/tri,px]],p/d'a,d/gal,log.j,pholm,p/rei,p/lon,p/sch,p/ura,p/tay,pkush,p/neu,p/mac,p/geh,p/ash,main,p/her,p/lev,p/bro,p/van,p/enc,p/hel,aug,p/hug,p/nea,p/pon,p/tsu,	1.32628150151E11	1.32628150151E11	[image, mu]	243	
semiminor	NUMBER	au {{,[[ast,au  (,.,,,au,	3.806671	6.98167	[semimajor]	170	
min_speed	OTHER	-1	-1.0	-1.0	[max_speed]	0	
next_p	OTHER	-aug-,-dec-,june,jan.,-nov-,march,-may-,-jul-,-jun-,oct.,-sep-,may,april,-feb-,-apr-,-mar-,,,apr.,-oct-,mar.,s,-jan-,(lost,febru,?,.'',novem,	-1.0	-1.0	[]	175	
aphelion	NUMBER	au {{,[[ast,.,,,au,	4.856927	4.856927	[]	159	

