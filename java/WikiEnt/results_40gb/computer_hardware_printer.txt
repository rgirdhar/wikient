weight	NUMBER	lb,.,	-1.0	-1.0	[]	5	
rom	NUMBER	mb,kb,	300.0	600.0	[dpi]	5	
speed	NUMBER	page,pages,lines,chara,	-1.0	-1.0	[]	5	
minimum	NUMBER	&nbsp,mb,.,	-1.0	-1.0	[maximum]	6	
image	OTHER	-,	-1.0	-1.0	[]	4	
caption	STRING	-1	-1.0	-1.0	[]	1	
frequency	NUMBER	mhz,	-1.0	-1.0	[]	2	
type	OTHER	color,/,	-1.0	-1.0	[name]	12	
ports	OTHER	]],,,c]],	-1.0	-1.0	[cost]	9	
introduced	OTHER	,,	-1.0	-1.0	[]	5	
power	NUMBER	&nbsp,watts,	1.0	4.0	[color]	8	
dimensions	NUMBER	.,x,	-1.0	-1.0	[]	6	
discontinued	OTHER	,,	-1.0	-1.0	[]	5	
language	OTHER	line,]],	-1.0	-1.0	[]	4	
slot	OTHER	-1	-1.0	-1.0	[]	4	
processor	OTHER	]],	-1.0	-1.0	[]	2	

