architects	OTHER	-1	-1.0	-1.0	[]	12	
nicknames	STRING	-1	-1.0	-1.0	[]	12	
poolnames	OTHER	pool,m-bas,etc,m poo,-,	-1.0	-1.0	[poolname]	18	
city	OTHER	-1	-1.0	-1.0	[]	44	
logocaption	OTHER	summe,.,. [[r,	-1.0	-1.0	[location]	19	
name	STRING	-1	-1.0	-1.0	[]	45	
length	NUMBER	|m|ab,metre,&nbsp,etc,|m|,m,.,	-1.0	-1.0	[]	24	
capacity	NUMBER	(,m poo,,,	2500.0	2500.0	[]	34	
closed	OTHER	-1	-1.0	-1.0	[]	0	
longitude	OTHER	-1	-1.0	-1.0	[latitude]	0	
deep	NUMBER	&ndas,ft,m,.,-,	-1.0	-1.0	[]	11	
built	OTHER	&ndas,– sep,s,-,	-1.0	-1.0	[]	15	
coordinates	NUMBER	|,.,	-1.0	-1.0	[]	3	
lanes	OTHER	–,,or,lanes,-,	-1.0	-1.0	[]	28	
fullname	STRING	-1	-1.0	-1.0	[]	31	
opened	OTHER	,may,july,,,novem,	-1.0	-1.0	[]	33	
logosize	NUMBER	px,	-1.0	-1.0	[]	1	
logo	STRING	-1	-1.0	-1.0	[]	2	
tenants	OTHER	summe,europ,	-1.0	-1.0	[]	3	
width	NUMBER	&nbsp,|m|,m,.,y,	-1.0	-1.0	[]	15	
previousname	STRING	-1	-1.0	-1.0	[]	1	
imagesize	NUMBER	px,	-1.0	-1.0	[]	15	
image	OTHER	water,april,).jpg,panaq,.jpg,aquat,olymp,	-1.0	-1.0	[]	34	
postcode	NUMBER	-1	600032.0	600032.0	[]	1	
cost	NUMBER	billi,milli,(,.,,,	-1.0	-1.0	[]	7	
footnotes	OTHER	-1	-1.0	-1.0	[]	8	
clubs	OTHER	]] [[,	-1.0	-1.0	[]	13	
address	NUMBER	, [[i,	-1.0	-1.0	[]	1	
demolished	OTHER	-1	-1.0	-1.0	[]	0	
owner	STRING	-1	-1.0	-1.0	[]	2	

