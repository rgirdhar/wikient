profession	STRING	-1	-1.0	-1.0	[]	5	
religion	OTHER	-1	-1.0	-1.0	[]	159	
rank	OTHER	-1	-1.0	-1.0	[map]	4	
feast_day	OTHER	febru,	-1.0	-1.0	[]	2	
married	NUMBER	-1	1958.0	1958.0	[]	1	
image_name	OTHER	px]],	-1.0	-1.0	[]	2	
spouse	STRING	–,—),) lad,	-1.0	-1.0	[]	31	
began	NUMBER	-1	1550.0	1550.0	[]	1	
signature	OTHER	.png,	-1.0	-1.0	[]	218	
cardinal	OTHER	-1	-1.0	-1.0	[]	0	
beatified_by	OTHER	-1	-1.0	-1.0	[]	0	
church	OTHER	-1	-1.0	-1.0	[]	17	
archbishop_of	OTHER	nd [[,st [[,	-1.0	-1.0	[]	4	
archdiocese	OTHER	-1	-1.0	-1.0	[]	13	
shrine	OTHER	-1	-1.0	-1.0	[rite]	1	
motto	STRING	-1	-1.0	-1.0	[]	8	
Proclamation	NUMBER	april,	-1.0	-1.0	[]	1	
coat_of_arms	STRING	-1	-1.0	-1.0	[]	3	
bishops	OTHER	st bi,nd  b,th [[,	-1.0	-1.0	[bishop_of]	229	
predecessor	OTHER	to,.,ad un,,,rd ba,-,	-1.0	-1.0	[]	227	
infobox_width	NUMBER	px,	-1.0	-1.0	[]	2	
opposed	OTHER	-1	-1.0	-1.0	[]	0	
caption	STRING	portr,th ch,passp,greek,.,), bi,	-1.0	-1.0	[]	49	
diocese	OTHER	–,	-1.0	-1.0	[]	150	
Title	STRING	-1	-1.0	-1.0	[]	1	
venerated_in	OTHER	-1	-1.0	-1.0	[venerated]	1	
residence	OTHER	-1	-1.0	-1.0	[]	27	
ordinated_by	OTHER	-1	-1.0	-1.0	[]	6	
beatified_place	OTHER	-1	-1.0	-1.0	[beatified_date]	0	
successor	OTHER	),st ba,-,	-1.0	-1.0	[]	194	
metropolis	OTHER	-1	-1.0	-1.0	[]	3	
enthroned	OTHER	janua,may,april,june,th,,th se,,,-,march,augus,septe,feb,octob,july,st of,febru,(aude,novem,x,	-1.0	-1.0	[]	237	
birth_name	STRING	-1	-1.0	-1.0	[]	62	
Bishop	NUMBER	april,	-1.0	-1.0	[]	1	
ended	OTHER	may,april,june,decem,,,augus,march,septe,octob,july,febru,(aude,novem,	-1.0	-1.0	[Order]	217	
type	OTHER	–,),  [,	-1.0	-1.0	[tomb, title, see]	146	
appointed	OTHER	may,decem,,,	-1.0	-1.0	[]	4	
death_place	OTHER	–,px]],/,,,	-1.0	-1.0	[birth_place]	407	
term_end	OTHER	march,	-1.0	-1.0	[]	2	
province	OTHER	of th,px|bo,	-1.0	-1.0	[]	79	
saint_title	OTHER	-1	-1.0	-1.0	[]	0	
dead	STRING	-1	-1.0	-1.0	[de]	2	
death_date	NUMBER	janua,may,april,june,decem,,,-,augus,march,septe,octob,july,|,th ju,febru,th oc,novem,	915.0	1986.0	[birth_date]	438	
period	NUMBER	–,- pre,,,	-1.0	-1.0	[Period]	3	
footnotes	OTHER	-1	-1.0	-1.0	[]	10	
alma_mater	OTHER	, m.a,, m.s,}}) p,}},)  st,),	-1.0	-1.0	[]	71	
predecessor2	STRING	-1	-1.0	-1.0	[]	1	
issues	OTHER	-1	-1.0	-1.0	[]	2	
elected	NUMBER	-1	1998.0	1998.0	[]	1	
canonized_place	OTHER	-1	-1.0	-1.0	[canonized_date]	0	
term_start	OTHER	-1	-1.0	-1.0	[]	0	
day	NUMBER	–,|mont,	-1.0	-1.0	[alt]	2	
signature_alt	OTHER	-1	-1.0	-1.0	[]	0	
parents	STRING	&ndas,	-1.0	-1.0	[]	22	
other	OTHER	-1	-1.0	-1.0	[order2]	2	
quashed	OTHER	-1	-1.0	-1.0	[]	0	
canonized_by	OTHER	-1	-1.0	-1.0	[]	0	
children	OTHER	(kath,sons,) ell,	-1.0	-1.0	[]	21	
ocupation	OTHER	-1	-1.0	-1.0	[occupation]	10	
patronage	OTHER	-1	-1.0	-1.0	[]	0	
name	STRING	(crop,.png,oct,js.jp,passp,.jpg,hq).j,-gosm,),-,	-1.0	-1.0	[image]	433	
buried	OTHER	at an,	-1.0	-1.0	[]	84	
coat_of_arms_alt	OTHER	-1	-1.0	-1.0	[]	0	
major_shrine	OTHER	-1	-1.0	-1.0	[]	0	
consecrated_by	OTHER	-1	-1.0	-1.0	[]	11	
imagesize	NUMBER	px,	-1.0	-1.0	[image_size]	30	
previous_post	NUMBER	-,	-1.0	-1.0	[]	1	
suffix	OTHER	-1	-1.0	-1.0	[prefix]	164	
consecration	NUMBER	janua,may,as bi,april,june,decem,jun,,,([[bi,march,augus,septe,nov,octob,july,|,febru,novem,	1294.0	1997.0	[]	159	
nationality	OTHER	-1	-1.0	-1.0	[]	107	
ordination	OTHER	may,april,([[de,june,) pri,by [[,decem,,,as pr,–,by ma,augus,march,dec,septe,july,|,st of,febru,[[dea,novem,	-1.0	-1.0	[]	188	
term	NUMBER	,,	-1.0	-1.0	[]	1	
color	OTHER	-1	-1.0	-1.0	[]	0	
other_post	OTHER	–,&ndas,&mdas,-,	-1.0	-1.0	[]	90	
attributes	OTHER	-1	-1.0	-1.0	[]	0	
successor2	STRING	-1	-1.0	-1.0	[]	1	
suppressed_date	OTHER	-1	-1.0	-1.0	[]	1	

