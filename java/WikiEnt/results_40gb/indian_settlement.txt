pushpin_label_position	STRING	-1	-1.0	-1.0	[]	1	
subdivision_type	STRING	-1	-1.0	-1.0	[]	1	
longd	NUMBER	-1	27.37	75.57	[latd]	2	
area_telephone	OTHER	-1	-1.0	-1.0	[]	0	
state_name	STRING	-1	-1.0	-1.0	[]	1	
sex_ratio	OTHER	-1	-1.0	-1.0	[]	0	
longEW	STRING	-1	-1.0	-1.0	[]	1	
name	STRING	-1	-1.0	-1.0	[]	1	
leader_name	OTHER	-1	-1.0	-1.0	[]	0	
coordinates_display	SET_OF_STRINGS	-1	-1.0	-1.0	[]	1	
subdivision_name	OTHER	-1	-1.0	-1.0	[]	1	
district	OTHER	-1	-1.0	-1.0	[]	1	
settlement_type	STRING	-1	-1.0	-1.0	[]	1	
vehicle_code_range	OTHER	-1	-1.0	-1.0	[]	0	
latNS	STRING	-1	-1.0	-1.0	[]	1	
pushpin_map	STRING	-1	-1.0	-1.0	[]	1	
area_magnitude	STRING	-1	-1.0	-1.0	[]	1	
area_total	OTHER	-1	-1.0	-1.0	[]	0	
website	OTHER	-1	-1.0	-1.0	[]	0	
population_as_of	NUMBER	-1	2001.0	2001.0	[]	1	
footnotes	OTHER	-1	-1.0	-1.0	[]	0	
unlocode	OTHER	-1	-1.0	-1.0	[]	0	
pushpin_map_caption	STRING	-1	-1.0	-1.0	[]	1	
population_total	NUMBER	-1	22928.0	22928.0	[]	1	
leader_title	OTHER	-1	-1.0	-1.0	[]	0	
postal_code	OTHER	-1	-1.0	-1.0	[]	0	
native_name	OTHER	-1	-1.0	-1.0	[]	0	
elevation_m	NUMBER	-1	480.0	480.0	[]	1	

