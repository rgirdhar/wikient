telescope9_type	OTHER	,-m te,foot,chere,",,&nbsp,m rad,×&nbs,cm sc," sch," (,"," sct,½&nbs,plus,cm wi,m iac,wide,' rad,.,x [[l,/,-m so,-,m [[i,cm,cm te,-cm r," sc,in mi,m "hi,m ref,in (,-inch,dual-,ed," ref,-metr,in ne,m ''(,in [[,in re,cm (,mm co,mm ha,m sch," f,×," mea," [[c,mm," [[d," dob,m sub,inch,mm (,cm ro,inche,cm [[,mm [[," pri," [[r,cm ne,metre," [[s,m (~," [[n,s,cm ce," new,cm ca,cm re,	-1.0	-1.0	[telescope10_type, telescope11_type, telescope15_type, telescope14_type, telescope5_type, telescope7_type, telescope8_type, telescope3_type, telescope6_type, telescope16_type, telescope4_type, telescope13_type, telescope12_type, telescope17_type, telescope2_type, telescope9_name]	661	
telescope17_name	STRING	-1	-1.0	-1.0	[]	2	
altitude	NUMBER	|m|ab,|ft|m,m (,',&nbsp,feet,m,.,m /,|m|ft,,,m,,meter,|ft}},(feet,	195.0	195.0	[]	238	
telescope8_name	OTHER	," tel,teles,-m te,obser,(u/c),&nbsp,m rad,refle,-cm (,mm so,]]gps,]],-inch," ref," f/,[[sch,-metr,cm sc,fitz,mm sa,m tel," mea,",-mete,inch,- "ur,(,m,),.,,,m dis,metre,]] gp,t,]] sc,- "ve,cm te," cpc," new,intel,refra,- "ma," cla,act,(×,	-1.0	-1.0	[telescope3_name, telescope5_name, telescope4_name, telescope6_name, telescope7_name]	718	
refnum	OTHER	-1	-1.0	-1.0	[]	1	
aircraft_type	OTHER	-1	-1.0	-1.0	[aircraft_name]	2	
weather	OTHER	clear,% cle,% of,night,.,corde,	-1.0	-1.0	[]	30	
head	STRING	-1	-1.0	-1.0	[]	1	
opened	DATE	-1	-1.0	-1.0	[]	1	
telescope21_name	NUMBER	m rad,.,	-1.0	-1.0	[telescope20_name]	6	
affiliations	OTHER	-1	-1.0	-1.0	[]	0	
aircraft_status	STRING	-1	-1.0	-1.0	[]	1	
num_volunteers	OTHER	-1	-1.0	-1.0	[]	0	
image	NUMBER	px|va,rifug,wlase,px|ma,a.jpg,px|mc,px]],.gif|,px|sh,-npac,px|do,m&,px|ha,_,px|mo,px|wi,.jpg|,px|ea,wp.jp,th lo,px|th,omm.j,.,px|ki,inchh,-,px|gl,px|ae,px|dr,ghz r,px|pu,px|]],px|tw,px|ch,px|ro,px|fr,px|pl,px|vi,night,px|no,view,px|ce,.png|,small,px|ve,pix.j,b-s.j,	-1.0	-1.0	[]	179	
image_border	OTHER	-1	-1.0	-1.0	[]	0	
country	OTHER	-1	-1.0	-1.0	[]	1	
num_staff	OTHER	-1	-1.0	-1.0	[]	0	
aircraft_manufacturer	OTHER	-1	-1.0	-1.0	[]	1	
webpage	STRING	-1	-1.0	-1.0	[]	1	
bgcolour	OTHER	",	-1.0	-1.0	[]	6	
telescope24_type	STRING	-1	-1.0	-1.0	[telescope23_type]	13	
budget	OTHER	-1	-1.0	-1.0	[]	0	
state	OTHER	-1	-1.0	-1.0	[size]	2	
remarks	OTHER	-1	-1.0	-1.0	[]	0	
organization	OTHER	–,	-1.0	-1.0	[]	337	
name	STRING	;brie,;s ob,	-1.0	-1.0	[]	404	
map	OTHER	-1	-1.0	-1.0	[]	0	
closed	OTHER	s,}},|,-,	-1.0	-1.0	[]	18	
parent_organization	OTHER	-1	-1.0	-1.0	[]	1	
mcaption	OTHER	-m te,obser,|in|m,, wit,maybe,park,|dryd,th fl,&nbsp,|mi|k,km fr,% ope,),.,-inch,m dis,-,e. wa,metre,gala,durin,cm te,	-1.0	-1.0	[location]	510	
website	OTHER	lpps,hills,&item,.htm,}},.html,=http,;brie,m ra-,.,.pane,/,.htm|,offic,.htm},.plal,/&dat,.as.a,=,	-1.0	-1.0	[]	341	
established	OTHER	,may,}},th,,(,(berk,,,-,}} -,s,octob,m ope,|,	-1.0	-1.0	[]	239	
telescope1_type	NUMBER	&nbsp,m rad,m opt,cm, f,-cm [," cas," alv,[[sch,meter,cm sc,|in|c," sch," (,-mete," ast,.,/,m, op,m [[g,m [[h,cm,cm te,in mi,|mm|,inc.,m ref,½ inc,-m an,m (,in (,” dia,-m [[,-inch,cm do," f/," ref,-m, f,in [[,cm (," f,|lx,cm da," [[c," and,inch,mm (,cm [[,cm bo,m dis," [[r,m mov," [[s,metre,cm ne,m (~," [[n,cm ri," new,cm ca,-elem,cm re,x,	-1.0	-1.0	[]	295	
abbreviation	STRING	-1	-1.0	-1.0	[]	1	
telescope	NUMBER	.,	-1.0	-1.0	[]	1	
headquarters	OTHER	-1	-1.0	-1.0	[]	1	
flight	NUMBER	/,	-1.0	-1.0	[]	1	
duration	NUMBER	to,	-1.0	-1.0	[]	1	
coords	NUMBER	(apo),, b,°,|,.,,,(unti,	9.0	981.0	[code]	511	
NASA	NUMBER	, oct,	-1.0	-1.0	[]	1	
background	STRING	-1	-1.0	-1.0	[]	1	
user	OTHER	-1	-1.0	-1.0	[]	1	

