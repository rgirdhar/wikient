manager	OTHER	-1	-1.0	-1.0	[]	0	
altitude	OTHER	-1	-1.0	-1.0	[]	0	
elevation_ft	NUMBER	to,&ndas,mts,+',m [[s,&nbsp,masl,m}},|m|ft,m to,m,,meter,|m|-,ft /,ft,ft (,m[htt,ft (d,|m|ab,|ft|m,',m (ma,.,[[met,/,m/,,,km,-,|m}},|-|,|ft|,fts,|to|,ft wh,ft. (,|ft|a,|–|,ft or,|km|a,m ahd,m (,m  (o,m -,m /,–,m, se,m]],m (pl,| m|f,| ft|,m abo,at co,m{{fa,mt,m asl,feet,m,[[abo,ft.,metre,|ft}},ft. a,|m|,|foot,|ft|-,	-105.0	5543.0	[elevation]	4319	
caption_bathymetry	STRING	,metre,n,°,). "[,),s''',]]. s,, sho,,,	-1.0	-1.0	[]	155	
designation	OTHER	-1	-1.0	-1.0	[]	10	
outflow	OTHER	named,other,inner,}},[[sto,sewag,unnam,(subt," tru,[[tri,, to,miljo,m³/s),rivul,l/sec,, jud,ft³/s,}} [[,strea,m,),.,canal,,,-,and,% [[l,|to|,%), [,%) [[,|km|m,|cuft,tribu,|usga,|,small,river,	-1.0	-1.0	[inflow]	5729	
villages	OTHER	-1	-1.0	-1.0	[]	1	
postal_code_type	OTHER	-1	-1.0	-1.0	[]	5	
temperature	OTHER	-1	-1.0	-1.0	[]	0	
time	NUMBER	.,	-1.0	-1.0	[]	3	
references	OTHER	|coyo,}},|p=}},|cany,and%,/sura,|luce,&text,|}},|goos,|soda,saska,|lost,|silv,|loch,|supe,|last,.htm,perce,|crys,|bicy,.,|nels,|loon,|lake,|pyra,|the,|cala,|summ,|,|kern,river,	-1.0	-1.0	[reference]	2287	
VOLUMEN	OTHER	-1	-1.0	-1.0	[]	0	
longs	NUMBER	,mts,|km}},|m|mi,&nbsp,|m|ft,meter,|mi|a,km or,mile,ft,|m|ab,|ft|k,|ft|m,| smi,miles,.,/,[[met,km,,,|m}},| mi|,|to|,|mi|,km  t,|yd}},|km|m,ft or,|ft|a,|km|a,mi (,kilom,km(,yd (,|mi}},mi or,km/,km,,yds,½ mil,mi,|yd|k,|m|yd,|mi|k,m,|yd|m,sq.km,yards,metre,km (,|ft}},|yd|a,|m|,	-92.9203	400.0	[lat_s, longm, lats]	2522	
long_EW	STRING	-1	-1.0	-1.0	[longEW]	2	
created	NUMBER	-1	1941.0	1941.0	[]	1	
discharge	NUMBER	-1	5905.0	5905.0	[]	1	
basin_countries_flag	NUMBER	-1	1.0	1.0	[]	1	
width_mi	NUMBER	to,kms,|km}},|m|mi,&nbsp," km,|m|ft,m to,meter,|mi|a,mile,ft,|m|ab,|ft|k,|ft|m,| smi,miles,.,/,km,,,|-|,-,|m}},mi at,|km|m,ft or,|ft|a,|km|a,mi (,kilom,mi (.,km(,yd (,km (m,km/,|mi}},km,,yds,mi,|yd|k,|m|yd,|mi|k,feet,m,|yd|m,yards,metre,km (,|ft}},|yd|a,|m|,m at,	0.08	130.0	[width]	2198	
map_caption	STRING	-1	-1.0	-1.0	[image_caption]	2	
number	NUMBER	-1	3920.0	3924.0	[]	2	
caption_lake	STRING	,, nor,&nbsp,=lake,]] (,]],|m|ft,, vie,after,|ny,]], o,''',[[nas,-i]]),). no,july,th au,; pic,provi,]], j,shala,(fals,indiv,).  t,image,]] sa,),.,. in,,,-,and,degre,road|,johnn,(ohio,flood,:,[[kan,|hwy,, aga,novem,]],,is vi,, nea,by [[,. the,road,from,% cap,.  no,–,(nq w,(july,-metr,engra,map o,. [[l,bridg,. ima,]] ne,chris,(hait,, aft,(top,upgra,with,may,, wit,, the,erupt,ekl.p,, loo,nd,,metre,]] im,augus,of th,|acre,s,). mo,octob,. gri,at hi,th ce,. wha,	-1.0	-1.0	[]	2101	
elevation_imperial	NUMBER	-1	269.0	11443.0	[]	2	
image_lake_size	NUMBER	px,	-1.0	-1.0	[]	1	
population_total	NUMBER	-1	448.0	448.0	[]	1	
cities	OTHER	km),mi se,|km|,km to,r,|km|m,km aw,km fr,|km|a,miles,km. a,.,	-1.0	-1.0	[]	4731	
bottom	STRING	-1	-1.0	-1.0	[]	1	
basin_countries	OTHER	px]],% [[c,	-1.0	-1.0	[]	7828	
region	OTHER	-1	-1.0	-1.0	[]	1	
shore	NUMBER	|km}},&nbsp,|mile,|m|ft,.km,km {{,|mi|a,|km|,km, m,mile,km/,|mi}},mi,|m|ab,|mi|-,|smi|,|mi|k,miles,| smi,m,.,km,,,|km|f,|km|-,|mi|,|km|m,|ft|a,|km|a,mi (,kilom,	0.4	5678.0	[]	1073	
BILDBESCHREIBUNG	OTHER	-1	-1.0	-1.0	[]	0	
settlements	OTHER	-1	-1.0	-1.0	[]	1	
irish_name	STRING	-1	-1.0	-1.0	[]	1	
infoboxtitle	STRING	-1	-1.0	-1.0	[]	1	
sections	OTHER	-1	-1.0	-1.0	[]	60	
INSELN	OTHER	-1	-1.0	-1.0	[]	0	
timezone_DST	STRING	-1	-1.0	-1.0	[]	6	
area_note	OTHER	-1	-1.0	-1.0	[]	1	
lat_NS	STRING	-1	-1.0	-1.0	[latNS]	2	
dam_name	OTHER	-1	-1.0	-1.0	[]	12	
image_lake	OTHER	,-koni,).png,-demi,jez m,u.jpg,-linx,-yw.p,, jul,and a,, sta,.tif,- lak,reser,-by-e,bedok,.jpg|,&,. tmn,b.jpg,image,(,),.,-karj,-kain,,,-,).jpg,feb,- geo,trees,, sep,view,tl.pn,-sm.j,øvre,-west,ppx.j,kalho,phr.j,_rese,, oct,, bel,lac d,metu,dec,-sept,sunse,m.jpg,.jpeg,styhe,_,f,-e-,may,-.jpg,- (,jun,byphi,mount,r,-xii-,g-,nc,stygg,vinst,rd la,-lac,x,- sto,to,.albe,-shas,a.jpg,s.jpg,img,ra.jp,g.jpg,a cle,tanuk,panor,%bbt_,%a,july,bank,c.jpg,_web.,-koch,almad,mamry,lake,-luvu,.jl.j,near,flood,asgf.,th ju,) - f,ahmed,pix.j,_ff,.png,t.jpg,px]],-a.jp,.jpg,p.jpg,px.jp,nd,_,wangn,b-,m).jp,aeria,tata.,- gor,b oph,april,a).jp,-iv-,-mapl,- cop,bearl,augus,ha, h,sep,	-1.0	-1.0	[]	3843	
flooded	OTHER	}},|,-,	-1.0	-1.0	[]	27	
built	OTHER	to,&,}},- oct,]],(''re,–{{st,-,–,}}, [,s,'s,(cons,—,octob,|,bc,th ce,	-1.0	-1.0	[]	132	
photo_caption	STRING	-1	-1.0	-1.0	[]	1	
location_map	STRING	-1	-1.0	-1.0	[]	1	
Placa	STRING	-1	-1.0	-1.0	[]	1	
name_lake	STRING	px,ft,	-1.0	-1.0	[alt_lake]	32	
drainage	NUMBER	|km,	-1.0	-1.0	[]	1	
caption	STRING	-1	-1.0	-1.0	[]	1	
City	OTHER	-1	-1.0	-1.0	[]	2	
residence_time	NUMBER	to,hours,or,.,,,weeks,% pop,-,–,days,+ yea,month,years,week,	0.43	54.2	[]	206	
Valley_name	STRING	-1	-1.0	-1.0	[]	1	
SEEBREITE	OTHER	-1	-1.0	-1.0	[]	0	
pushpin_map_caption	STRING	-1	-1.0	-1.0	[]	1	
BESONDERHEITEN	OTHER	-1	-1.0	-1.0	[]	1	
pushpin_mapsize	NUMBER	-1	300.0	300.0	[]	1	
map_alt	OTHER	-1	-1.0	-1.0	[]	0	
club	OTHER	-1	-1.0	-1.0	[]	1	
volume	NUMBER	,|e,acre-,|gl|a,|impg,milli,|cuyd,|m|ft,|gl|m,|km,megal,|m,mil.,km³/,m³,cubic,|ml|i,.,km,,,-,billi,gl,|to|,|cuft,|usga,mio m,hm³,[[meg,mln m,}},cu m,|hm,hm,km³,|yd,gigal,galls,/ acr,ml,mm,e,млн.,|l|ac,m,|cumi,|gal|,|acre,hml,x,	0.78	7.6E10	[]	1134	
mansarovar	NUMBER	kaila,	-1.0	-1.0	[]	1	
fishing	STRING	-1	-1.0	-1.0	[]	1	
barangays	NUMBER	-1	33.0	33.0	[]	1	
location	OTHER	km so,}},&nbsp,|mi|k,km se,km no,block,km we,(buch,km(,, alb,t,, man,mile,|km|a,|,, new,km ea,, sas,	-1.0	-1.0	[]	7324	
perimeter	NUMBER	|km|m,km,,,	-1.0	-1.0	[]	5	
type	OTHER	t,: [[r,% sal,-,	-1.0	-1.0	[]	3370	
NGENGRAD	NUMBER	/,	-1.0	-1.0	[]	1	
transparency	NUMBER	.,	-1.0	-1.0	[]	4	
basin_population	NUMBER	,,	-1.0	-1.0	[]	1	
image_width	NUMBER	px,	-1.0	-1.0	[]	4	
province	OTHER	-1	-1.0	-1.0	[]	1	
longitude	NUMBER	-1	-81.21321	48.20662	[latitude]	2	
image2_lake	STRING	-1	-1.0	-1.0	[]	1	
population_density_sq_mi	NUMBER	-1	101.1	101.1	[]	1	
catchment	NUMBER	,|ha}},|e,mi² (,sq mi,e,&nbsp,|ha|a,|mi,.,,,|sqmi,km²,km,ha,|km,m by,km² (,|acre,sq km,|km²|,	3.4	6.8744433E7	[]	834	
align	STRING	-1	-1.0	-1.0	[]	9	
population_as_of	NUMBER	|,	-1.0	-1.0	[]	3	
period	NUMBER	,,	-1.0	-1.0	[]	1	
url	STRING	-1	-1.0	-1.0	[map]	3	
depth	NUMBER	to,m (,feet.,&nbsp,m  (o,m (su,|m|ft,m,,meter,m{{sf,|m|-,m  (e,|ft|y,ft,yds,ft (,m ({{,|m|ab,|ft|m,',mt,feet,m,.,+ m (,,,m/,|-|,|m}},-,fts,|ft|,ft.,metre,ft. (,|ft}},ft or,|ft|a,|km|a,|foot,|m|,m (or,	2.0	666.0	[]	2252	
nearest_city	OTHER	-1	-1.0	-1.0	[]	1	
population_density_km2	NUMBER	-1	111.3	111.3	[]	1	
country	OTHER	-1	-1.0	-1.0	[countries]	11	
islands_category	STRING	-1	-1.0	-1.0	[]	21	
map_width	OTHER	-1	-1.0	-1.0	[]	0	
webpage	STRING	-1	-1.0	-1.0	[]	1	
other_name	OTHER	-1	-1.0	-1.0	[]	3	
postal_code	NUMBER	-1	60948.0	60948.0	[]	1	
geolocalisation	STRING	-1	-1.0	-1.0	[]	1	
owner	OTHER	-1	-1.0	-1.0	[]	1	
islands	OTHER	squar,([[as,(mill,(grot,+ ([[,(smal,&nbsp,(hali,(orig,(levs,unnam,(''in,|m|ft,(incl,{{fac,|km,(off,(île,; [[p,([[ry,plus,islet,(grea,(aust,([[ki,or,(lewi,+]] (,+,(,, har,),.,,,(virm,-,(not,, sna,(no i,(tivo,islan,([[gr,([[ol,small,large,(rams,named,([[ma,([[ha,([[de,: ōji,bigge,(reve,(solo,([[po,, unn,|x|,(mike,inclu,(fron,(''no,(drak,(wört,(''se,([[in,|yd|m,(nehr,main,(unna,+ ''(,, man,([[is,([[be,([[ul,, maj,(cast,(wiza,) isl,(suga,th ce,varyi,perma,sunke,	-1.0	-1.0	[]	772	
alt_bathymetry	OTHER	px,	-1.0	-1.0	[]	4	
UMFANG	OTHER	-1	-1.0	-1.0	[]	0	
lake_name	STRING	degre,/draf,;asse,mile,rd ju,reser,;aigu,.,hamil,;s,& a-,	-1.0	-1.0	[Lake_name]	8052	
sealfile	OTHER	-1	-1.0	-1.0	[]	0	
scale	OTHER	|km|a,	-1.0	-1.0	[place]	4	
pushpin_label_position	STRING	-1	-1.0	-1.0	[]	1	
locatormapfile	OTHER	-1	-1.0	-1.0	[]	1	
remarks	STRING	-1	-1.0	-1.0	[]	1	
salinity	NUMBER	.,-,	-1.0	-1.0	[]	2	
frozen	OTHER	(aver,days,th an,month,.,/,,,-,	-1.0	-1.0	[]	1581	
max_depth	NUMBER	.,	-1.0	-1.0	[]	1	
TIEFE	OTHER	-1	-1.0	-1.0	[]	0	
area	NUMBER	,to,[[hec,|ha}},km²/,squar,sq. m,&nbsp,acre,[[km²,km² o,|km,meter,|mi|a,sq km,|m,|km²},|km²|,m²,km² t,*,.,km,,,|sqmi,|-|,-,ha,|to|,|ft|a,e+,|ha|a,|mi,ha.,[[acr,ha  {,km²,–,km² (,|mi}},acres,|sqkm,sq mi,hecta,e,m,ha {{,-acre,ha (m,km²]],|m²|a,|ha|m,|acre,|ha|k,km² –,ha]],ha (,|ha|s,x,	0.01	8350.0	[]	4449	
shipwrecks	NUMBER	(powe,	-1.0	-1.0	[]	1	
name	STRING	-1	-1.0	-1.0	[]	1	
shoreline	NUMBER	miles,	-1.0	-1.0	[]	1	
BREITENGRAD	NUMBER	/,	-1.0	-1.0	[]	1	
coordinates	NUMBER	°,	-1.0	-1.0	[]	1	
length_mi	NUMBER	-1	1.1	1.3	[]	2	
sites	STRING	-1	-1.0	-1.0	[city]	4	
population_footnotes	OTHER	-1	-1.0	-1.0	[]	0	
pushpin_map	STRING	-1	-1.0	-1.0	[]	2	
image_bathymetry	STRING	r.jpg,.png,area.,).jpg,-demi,.jpg,l upp,damer,-,	-1.0	-1.0	[]	200	
Website	STRING	-1	-1.0	-1.0	[]	1	
website	OTHER	-1	-1.0	-1.0	[]	6	
BEZUG	STRING	-1	-1.0	-1.0	[]	1	
fishable_miles	NUMBER	-1	4.0	4.0	[]	1	
Basin_country	OTHER	-1	-1.0	-1.0	[]	2	
imagesize	NUMBER	px,	400.0	400.0	[image_size]	8	
agency	OTHER	-1	-1.0	-1.0	[]	60	
photo	STRING	-1	-1.0	-1.0	[]	2	
basin_districts	OTHER	-1	-1.0	-1.0	[]	3	
coords	NUMBER	º,degre,.|,|s|,°,|,|-,}}{{c,.,|n|,	-1.0	-1.0	[]	7255	
NAHESTADT	OTHER	-1	-1.0	-1.0	[]	1	
districts	NUMBER	th di,	-1.0	-1.0	[]	1	
area_scale	NUMBER	-1	5.0	303.0	[area_acre]	4	
group	OTHER	-1	-1.0	-1.0	[]	55	
SEELAENGE	OTHER	-1	-1.0	-1.0	[]	0	

