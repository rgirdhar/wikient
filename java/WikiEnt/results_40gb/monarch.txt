consortto	OTHER	?–,) mar,other,of ca,june,) joa,. nab,. naa,– ''c,–×,-,–,to de,st ro,st co,st wi,wives,.lady,th ce,) [[i,. lad,	-1.0	-1.0	[consorts, consort]	438	
for	NUMBER	]], s,	-1.0	-1.0	[]	1	
grandfather	OTHER	-1	-1.0	-1.0	[]	2	
grandad	OTHER	-1	-1.0	-1.0	[]	1	
religion	OTHER	and a,}},|p=,	-1.0	-1.0	[Religion]	453	
rank	NUMBER	bc|df,th,	-1.0	-1.0	[date]	4	
married	STRING	-1	-1.0	-1.0	[]	5	
realm	STRING	-1	-1.0	-1.0	[alt]	4	
feast	DATE	-1	-1.0	-1.0	[]	1	
movement	OTHER	-1	-1.0	-1.0	[]	1	
focus	STRING	-1	-1.0	-1.0	[]	1	
influenced	OTHER	-1	-1.0	-1.0	[]	1	
place	STRING	px]]g,);  t,);  m,s) fl,	-1.0	-1.0	[name, law, Name, State]	1576	
fullname	OTHER	-1	-1.0	-1.0	[]	0	
signature	OTHER	px]],).svg,	-1.0	-1.0	[]	103	
othrtitles	STRING	-1	-1.0	-1.0	[othrtitle, othertitle]	7	
reign3	NUMBER	bc-ca,&ndas,- jan,bce &,until,ce-,(bava,– oct,– aug,– jan,–late,– unk,bc/,ce -,bc-,– sep,{{spa,..-,or be,bc,,or,-c.,s-ear,(,– pre,/,,,(with,th no,-,;,– aft,febru,novem,maldi,nd ce,– jun,– jul,bc –,– ''c,{{sfn,–,bc -c,to ab,—,–c.,-pres,th da,}} th,(agai,rd to,bc–,janua,may,–c,nd da,bce,bc|,s,octob,th ce,(date,x,rd ce,, c.,to,&nbsp,bc to,bc (,-june,bc -,march,– ? b,july,bc,s or,- feb,bc --,- pre,th,,th-,and,ce (s,''c.',- abo,bc]]-,ce,– apr,bce –,a.d.,bce]],or ju,bc]],june,]]-[[,ce to,decem,bc&nd,– c.,bce t,–[[ci,septe,ce –,- c.,,  so,–octo,bce o,– ca.,s - c,-?,april,– ?,– nov,s-,– may,- sep,augus,ad,	269.0	1984.0	[reign2, reign1, reign]	1797	
birth_date	OTHER	st ra,bc]],}},june,-falg,decem,([[an,–,(prob,march,septe,bc or,th da,july,th of,bc,st ce,(?),may,janua,th ap,april,or,(,.,th [[,/,: bet,,,bce,-,zulqa,mecca,augus,s,octob,or ea,ad,~,, bha,|,?,febru,ce,th ce,(date,novem,	-1.0	-1.0	[]	1057	
death	OTHER	to,,,	-1.0	-1.0	[birth, date1]	13	
son	OTHER	, d.,) mar,other,bc),)voic,)irin,),.,. chr,. lau,-,–,march,) voi,) nea,)smar,, div,, dau,th ce,	-1.0	-1.0	[of, link, king, clan]	316	
Nationality	STRING	-1	-1.0	-1.0	[]	1	
prefix	OTHER	-1	-1.0	-1.0	[]	1	
funeral	STRING	-1	-1.0	-1.0	[]	1	
cremation	OTHER	june,''as,&nbsp,decem,as co,, [[t,march,in [[,septe,th da,july,bc, [,bc,,(at,, [[d,may,janua,april,or,, [[m,, [[p,/,bce,,,and,, [[r,-,augus,octob,ad,febru,ce,, [[a,th ce,, [[b,, [[c,novem,(date,	-1.0	-1.0	[coronation]	305	
titles	OTHER	to,,th pa,}},px]],th ki,–,.svg|,nd [[,—,st [[,st pa,nd no,st em,bce–c,nd em,th em,th la,th lo,st no,),th [[,th no,-,st ru,rd no,rd [[,rd em,st ma,	-1.0	-1.0	[Titles, title, Title]	2039	
9	STRING	-1	-1.0	-1.0	[6, 8]	12	
motto	STRING	-1	-1.0	-1.0	[]	1	
coronation2	OTHER	-1	-1.0	-1.0	[coronation1]	0	
uncle	OTHER	-1	-1.0	-1.0	[]	1	
death_year	NUMBER	-1	1944.0	1944.0	[]	1	
spouses	OTHER	st  d,) [[a,more.,queen,) mal,}},px]],st dy,),nd  d,/,,,bc -,–,) an,) taj,nd  s,bride,th  d,th ce,px]][,	-1.0	-1.0	[spouse4, spouse5]	964	
baptism	NUMBER	or la,	-1.0	-1.0	[]	1	
Caption	STRING	-1	-1.0	-1.0	[]	1	
native_lang8	STRING	-1	-1.0	-1.0	[]	10	
othertitles	OTHER	)   p,–,th pa,. sul,.htm,provo,px|li,th [[,,,-,	-1.0	-1.0	[]	273	
offspring	OTHER	child,other,) [[s,sons,th dy,,,|p=,{{sfn,	-1.0	-1.0	[]	135	
native_lang5	OTHER	}},	-1.0	-1.0	[native_lang4]	200	
caption	OTHER	, let,ad),-[[st,perio,nd ea,|lint,, rep,bc, h,}}.,, pai,a.c.,"saxo,and c,''[[n,st pa,th ye,) at,, dur,or,th-,'',ce–,[[min,),.,/,is k',. (d.,-,bc. i,th dy,st  y,origi,depic,''gos,: pho,th-ce,th ea,publi,st da,by ra,th  y,saxon,by [[,[[rec,from,–,, a b,septe,rupia,[[sak,×,st ce,, wit,rd&nb,th&nb,s.,) has,s),th [[,. [[s,portr,nd du,th pr,s,p,book,insid,as ob,]] ma,th ce,).,x,	-1.0	-1.0	[]	873	
concubine	OTHER	-1	-1.0	-1.0	[]	1	
burial_date	OTHER	bc,,,bce,	-1.0	-1.0	[]	4	
Birth	NUMBER	-1	1920.0	1920.0	[]	1	
field	OTHER	-1	-1.0	-1.0	[]	2	
residence	OTHER	-1	-1.0	-1.0	[]	0	
native_lang2_name2	OTHER	-1	-1.0	-1.0	[native_lang1_name1]	93	
brother	STRING	-1	-1.0	-1.0	[]	1	
ethnicity	OTHER	-1	-1.0	-1.0	[Ethnicity]	12	
beliefs	OTHER	-1	-1.0	-1.0	[]	1	
birth_name	STRING	-1	-1.0	-1.0	[]	4	
regent2	OTHER	–,}}  (,	-1.0	-1.0	[regent, regent1]	8	
interregnum	OTHER	-1	-1.0	-1.0	[]	0	
Sultanate	OTHER	-1	-1.0	-1.0	[]	1	
native_lang8_name1	STRING	-1	-1.0	-1.0	[native_lang5_name1]	45	
known_for	OTHER	-1	-1.0	-1.0	[]	1	
death_place	OTHER	,&ndas,&nbsp,{{pag,bce (,bc}},]],bc (a,may?,,march,| | |,, {{d,bc or,july,,july,bc,(?),|p=,june,,km so,or,+,(,.,/,,,and,-,in mo,, age,(a.d.,?,ce,febru,a.d.,th-ce,novem,a.d,bc]],june,}},[aged,,/sep,decem,|a}},ce (a,(age,{{sfn,–,|}},safar,septe,b.c.e,c.e (,(died,(aged,, or,may,janua,ad (a,bc  o,april,, may,(befo,}} (s,nd da,th [[,bce,c.,)|car,augus,s,octob,ad,|,ramad,[[mot,or ja,th ce,/ feb,	-1.0	-1.0	[death_date, birth_place]	2903	
offsprings	NUMBER	st ma,	-1.0	-1.0	[]	1	
family	OTHER	-1	-1.0	-1.0	[]	2	
date2	NUMBER	(coro,) ''a,july,	-1.0	-1.0	[Notes]	3	
type2	STRING	st re,	-1.0	-1.0	[type1]	133	
other_names	STRING	-1	-1.0	-1.0	[]	2	
background_color	NUMBER	d,	-1.0	-1.0	[]	2	
occupation	OTHER	-1	-1.0	-1.0	[]	4	
networth	NUMBER	.,	-1.0	-1.0	[]	1	
size	NUMBER	px,	-1.0	-1.0	[]	1	
resting_place	OTHER	-1	-1.0	-1.0	[]	0	
predecessor1	OTHER	&ndas,th pa,) and,nd ea,[[trp,nd  d,kings,]],rd mo,–,st ti,bce–,nd  s,) [[l,) (fa,bc,rd ea,.e,st ea,th lo,rd lo,),)   ',-,th pr,month,th ce,th ea,	-1.0	-1.0	[predecessor]	1592	
death_cause	STRING	-1	-1.0	-1.0	[]	2	
predecessor3	STRING	-1	-1.0	-1.0	[predecessor2]	2	
mother	OTHER	rd ea,.e,nd so,rd wi,of ca,}},st ea,th lo,s&nbs,rd lo,),st ba,,,-,)]],–,}} of,), pr,)|isa,th ce,), da,th ea,|p=,	-1.0	-1.0	[brothers, anthem, father]	2170	
investiture	OTHER	may,march,	-1.0	-1.0	[]	7	
relatives	OTHER	-1	-1.0	-1.0	[]	2	
resting_place_coordinates	OTHER	-1	-1.0	-1.0	[]	0	
parents	STRING	-1	-1.0	-1.0	[]	1	
horses	SET_OF_STRINGS	-1	-1.0	-1.0	[]	1	
crown	NUMBER	–,	-1.0	-1.0	[]	1	
issue	OTHER	,&ndas,) pad,-este,shaka,ac. c,v.jpg,d.jpg,nd ea,kopie,- mil,and d,- mih,simba,nadar,rd ea,b.jpg,),.,/,buste,,,_ad.p,-,morle,coins,).jpg,sons,,yr. o,) ala,annal,rd em,th ea,- la,st da,(titu,matee,peqch,ea,–,)|man,fol.,) dur,.jpeg,_,mp,monom,d,- rad,-.jpg,th lo,n,terte,)   w,o,)|ale,'s (w,nevit,pushk,v,r,)-coi,- vie,),,й_,th ce,) ism,) let,x,daugh,a.jpg,_arta,s.jpg,stele,- büs,by,st ba,bc.jp,- asu,) (se,posta,)   ',- pet,by gu,.png,other,.jpg,- pg,child,pt.jp,cdm p,ce.jp,) [[r,[[flo,.gif,), [[,) [[i,s),)   m,px,)  ma,- pos,sons,cleop,sep,	-1.0	-1.0	[Issue, image]	1573	
children	OTHER	–,sons,), [[,, inc,daugh,|p=,	-1.0	-1.0	[]	144	
husband	OTHER	-1	-1.0	-1.0	[]	1	
sinhala	OTHER	-1	-1.0	-1.0	[]	1	
given_name	OTHER	-1	-1.0	-1.0	[]	0	
buried	OTHER	to se,in th,]] th,, [[u,}},in [[,or,in ku,]], t,),]],	-1.0	-1.0	[burial]	859	
Khanate	OTHER	-1	-1.0	-1.0	[]	1	
consortreign	NUMBER	march,april,– aug,octob,	-1.0	-1.0	[]	4	
rites	STRING	-1	-1.0	-1.0	[]	1	
imgw	NUMBER	px,	150.0	150.0	[]	11	
imagesize	NUMBER	px,	-1.0	-1.0	[image_size]	41	
burial_placer	OTHER	|,),.,-d, s,	-1.0	-1.0	[burial_place]	129	
Children	STRING	-1	-1.0	-1.0	[]	1	
ruler	OTHER	. ing,}},queen,[[tha,ranis,	-1.0	-1.0	[queen]	127	
nationality	OTHER	-1	-1.0	-1.0	[]	6	
term	OTHER	',, pre,) [[n,), [[,-,	-1.0	-1.0	[heir, arms]	57	
wife	OTHER	-1	-1.0	-1.0	[]	7	
sibling	OTHER	-1	-1.0	-1.0	[]	1	
native_name	OTHER	-1	-1.0	-1.0	[]	1	
successor2	STRING	-1	-1.0	-1.0	[]	1	
successor3	OTHER	) [[e,&ndas,) and,}},) [[b,)|rag,kings,nd  d,]],–,depos,[[cal,septe,st ti,) (so,bc,rd ea,at el,survi,.e,st ea,),/,st lo,-,th pr,)),th ce,th ea,	-1.0	-1.0	[successor1]	1694	
dynasty	OTHER	th dy,px]],st dy,px|li,th of,px]][,nd of,	-1.0	-1.0	[]	635	

