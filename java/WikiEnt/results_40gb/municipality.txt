government_footnotes	STRING	-1	-1.0	-1.0	[]	4	
area_land_km2	OTHER	-1	-1.0	-1.0	[area_land_ha]	291	
p1	NUMBER	-1	1.0	75.0	[]	379	
elevation_footnotes	OTHER	-1	-1.0	-1.0	[]	2	
timezone1_DST	OTHER	-1	-1.0	-1.0	[]	335	
population_blank2_title	OTHER	-1	-1.0	-1.0	[]	1	
postal_code_type	OTHER	-1	-1.0	-1.0	[]	483	
seal_size	NUMBER	px,x,	-1.0	-1.0	[]	7	
area_footnotes	OTHER	/,	-1.0	-1.0	[]	290	
longs	NUMBER	-1	0.0	126.0	[longm, lats]	1873	
seat	OTHER	-1	-1.0	-1.0	[_sec]	328	
income	NUMBER	.,	-1.0	-1.0	[]	1	
blank3_info_sec3	SET_OF_STRINGS	-1	-1.0	-1.0	[]	1	
total_type	STRING	-1	-1.0	-1.0	[]	1	
utc_offset_DST	NUMBER	-1	2.0	3.0	[utc_offset1_DST]	337	
settlement_type	OTHER	baran,	-1.0	-1.0	[]	725	
parts_type	OTHER	-1	-1.0	-1.0	[]	381	
area_total_sq_mi	NUMBER	-1	12355.0	254827.0	[]	3	
official_name	STRING	-1	-1.0	-1.0	[]	88	
blank7_info	STRING	-1	-1.0	-1.0	[blank5_info, blank6_info]	5	
map_label	STRING	-1	-1.0	-1.0	[]	1	
image_seal	STRING	px-ob,idrij,.png,divac,sveti,kocev,bistr,crnom,.svg,dolen,koste,p%,naklo,.jpeg,loski,ajdov,	-1.0	-1.0	[image_map, image_alt, image_flag]	803	
shield_alt	STRING	-1	-1.0	-1.0	[]	1	
nickname	STRING	-1	-1.0	-1.0	[]	17	
map_caption	STRING	acg.j,_,th ce,	-1.0	-1.0	[image_caption]	450	
blank_info_sec3	OTHER	th  c,st  c,th cl,st cl,rd cl,st,nd cl,th,	-1.0	-1.0	[blank_info_sec2]	671	
coor_pinpoint	OTHER	-1	-1.0	-1.0	[]	11	
0	OTHER	-1	-1.0	-1.0	[]	0	
population_total	OTHER	,,	-1.0	-1.0	[population_note]	736	
motto	OTHER	-1	-1.0	-1.0	[]	15	
income_as_of	NUMBER	-1	2010.0	2010.0	[]	1	
elevation_m	NUMBER	-1	6.0	1200.0	[elevation_ft]	8	
timezone1	OTHER	]],]], [,	-1.0	-1.0	[timezone]	738	
pushpin_map_alt	OTHER	-1	-1.0	-1.0	[]	0	
zip_code	NUMBER	-,	2727.0	2727.0	[iso_code]	297	
subdivision_type5	OTHER	-1	-1.0	-1.0	[subdivision_type4, subdivision_type, subdivision_type3, subdivision_type2, subdivision_type1]	2249	
blank_name_sec3	OTHER	-1	-1.0	-1.0	[blank_name_sec2]	1002	
area_total_dunam	NUMBER	-1	4756.0	4756.0	[]	1	
leader_title9	OTHER	st di,	-1.0	-1.0	[leader_title8]	465	
blank1_info_sec1	NUMBER	-1	114.0	2584.0	[]	317	
leader_party2	OTHER	-1	-1.0	-1.0	[leader_party1, leader_party]	39	
timezone_DST	STRING	-1	-1.0	-1.0	[]	2	
deg	STRING	-1	-1.0	-1.0	[]	9	
area_note	DATE	-1	-1.0	-1.0	[]	289	
Motto	STRING	-1	-1.0	-1.0	[]	2	
Police	STRING	-1	-1.0	-1.0	[]	1	
blank2_info_sec3	STRING	-1	-1.0	-1.0	[]	1	
seal_alt	OTHER	-1	-1.0	-1.0	[map_alt, flag_alt]	0	
latNS	STRING	-1	-1.0	-1.0	[]	399	
shield_size	NUMBER	px,	-1.0	-1.0	[]	1	
coordinates_footnotes	NUMBER	°,	-1.0	-1.0	[]	1	
area_water_km2	OTHER	-1	-1.0	-1.0	[]	290	
meaning	OTHER	-1	-1.0	-1.0	[]	0	
registered_voters	NUMBER	,,	-1.0	-1.0	[]	1	
pushpin_map_caption	STRING	-1	-1.0	-1.0	[]	391	
unit_pref	STRING	-1	-1.0	-1.0	[]	347	
pushpin_mapsize	NUMBER	-1	200.0	200.0	[]	8	
established_date2	OTHER	s -,janua,(as a,]],,,]], [,and,	-1.0	-1.0	[established_date]	125	
type	OTHER	-1	-1.0	-1.0	[]	11	
population_metro	STRING	-1	-1.0	-1.0	[]	1	
blank7_name	STRING	-1	-1.0	-1.0	[blank6_name]	6	
leader_name9	STRING	,,	-1.0	-1.0	[leader_name8]	399	
blank2_info	DATE	-1	-1.0	-1.0	[]	1	
founder	OTHER	-1	-1.0	-1.0	[]	0	
population_as_of	OTHER	censu,janua,decem,|,,,	-1.0	-1.0	[]	737	
coordinates_region	STRING	-1	-1.0	-1.0	[]	23	
footnotes	OTHER	.,	-1.0	-1.0	[]	734	
population_density_km2	STRING	/,	-1.0	-1.0	[]	711	
coordinates_type	OTHER	nd_so,_type,)_reg,nd_re,nd,)_sou,rd_re,_scal,),st,rd,,,	-1.0	-1.0	[]	726	
population_demonym	STRING	-1	-1.0	-1.0	[]	19	
postal_code	NUMBER	-1	1900.0	9704.0	[]	258	
other_name	STRING	-1	-1.0	-1.0	[]	6	
population_blank2	SET_OF_STRINGS	-1	-1.0	-1.0	[]	1	
pushpin_label_position	STRING	-1	-1.0	-1.0	[]	16	
area_code	NUMBER	,]],	35.0	5632.0	[]	34	
image_shield	STRING	.svg,.jpeg,	-1.0	-1.0	[]	309	
tourism_slogan	STRING	-1	-1.0	-1.0	[]	1	
established_title	STRING	-1	-1.0	-1.0	[]	378	
longEW	STRING	-1	-1.0	-1.0	[]	399	
name	STRING	-1	-1.0	-1.0	[]	737	
coordinates_display	OTHER	-1	-1.0	-1.0	[]	731	
area_water_percent	NUMBER	-1	18.6	18.6	[]	1	
population_blank1	OTHER	-1	-1.0	-1.0	[]	0	
area_code_type	OTHER	-1	-1.0	-1.0	[]	393	
population_footnotes	OTHER	-1	-1.0	-1.0	[]	289	
image_skyline	OTHER	.jpg],juni,divac,.jpg,	-1.0	-1.0	[]	40	
pushpin_map	STRING	-1	-1.0	-1.0	[]	388	
website	OTHER	www.l,&cid=,www.m,www.n,.html,www.j,www.s,www.v,.,/,www.r,.aspx,.smed,.berg,.bbd,.ncc.,	-1.0	-1.0	[density]	371	
mapsize	NUMBER	px,	-1.0	-1.0	[imagesize]	19	
seat_type	OTHER	-1	-1.0	-1.0	[]	327	
registered_voters_as_of	NUMBER	-1	2010.0	2010.0	[]	1	
native_name_lang	STRING	-,	-1.0	-1.0	[]	726	
parts_style	STRING	-1	-1.0	-1.0	[]	381	
subdivision_name3	OTHER	th di,nd di,),]],th_di,st di,st_di,st (t,nd_di,st,th  d,rd_di,rd di,	-1.0	-1.0	[subdivision_name, subdivision_name1, subdivision_name2]	2237	
subdivision_name4	NUMBER	-1	15.0	53.0	[]	6	
area_total_km2	OTHER	,,	-1.0	-1.0	[]	704	
native_name	OTHER	-1	-1.0	-1.0	[]	398	
government_type	OTHER	-1	-1.0	-1.0	[]	0	
subdivision_name5	STRING	-1	-1.0	-1.0	[]	1	
area_total_ha	NUMBER	,,	-1.0	-1.0	[]	4	
population_blank1_title	STRING	-1	-1.0	-1.0	[]	5	
utc_offset1	NUMBER	-1	1.0	8.0	[utc_offset]	734	

