successor	OTHER	-1	-1.0	-1.0	[]	36	
spouse3	STRING	–,&ndas,st);,	-1.0	-1.0	[spouse1, spouse2, spouse]	93	
hazzan	OTHER	-1	-1.0	-1.0	[]	1	
birth_name	STRING	-1	-1.0	-1.0	[]	78	
profession	STRING	-1	-1.0	-1.0	[]	5	
ended	OTHER	march,janua,,,novem,	-1.0	-1.0	[]	45	
religion	STRING	-1	-1.0	-1.0	[]	5	
known_for	OTHER	-1	-1.0	-1.0	[]	0	
term_end	NUMBER	,,	-1.0	-1.0	[]	1	
image_size	NUMBER	px,	-1.0	-1.0	[]	4	
yeshivaposition	OTHER	-1	-1.0	-1.0	[]	67	
dynastyname	OTHER	-1	-1.0	-1.0	[]	0	
synagogue	OTHER	easte,	-1.0	-1.0	[]	34	
other_names	STRING	-1	-1.0	-1.0	[]	1	
synagogueposition	STRING	-1	-1.0	-1.0	[]	27	
denomination	OTHER	-1	-1.0	-1.0	[]	111	
signature	OTHER	-1	-1.0	-1.0	[]	84	
work	OTHER	,,	-1.0	-1.0	[rank, term]	7	
death_date	NUMBER	iyar,janua,may,april,[[iya,june,decem,,,(age,augus,rd,,st,,septe,octob,july,|,febru,tishr,novem,	1756.0	1989.0	[]	102	
occupation	OTHER	-1	-1.0	-1.0	[]	36	
yeshiva	OTHER	-1	-1.0	-1.0	[]	77	
semikhah	OTHER	-1	-1.0	-1.0	[semikha]	47	
footnotes	OTHER	-1	-1.0	-1.0	[]	1	
alma_mater	OTHER	-1	-1.0	-1.0	[]	46	
resting_place	OTHER	-1	-1.0	-1.0	[]	1	
Doctorate	OTHER	-1	-1.0	-1.0	[]	1	
mother	STRING	-1	-1.0	-1.0	[father]	2	
prefix	STRING	-1	-1.0	-1.0	[]	155	
organisationposition	STRING	-1	-1.0	-1.0	[]	27	
term_start	NUMBER	-1	1933.0	1933.0	[]	1	
relatives	OTHER	-1	-1.0	-1.0	[]	0	
resting_place_coordinates	NUMBER	.,	-1.0	-1.0	[]	1	
parents	STRING	-1	-1.0	-1.0	[]	73	
organizationposition	OTHER	-1	-1.0	-1.0	[organisationposistion]	11	
synagogueposistion	OTHER	-1	-1.0	-1.0	[]	0	
employer	OTHER	-1	-1.0	-1.0	[]	5	
children	OTHER	child,–,&ndas,girls,other,)  sa,sons,,[[bin,daugh,	-1.0	-1.0	[]	63	
predecessor	OTHER	-1	-1.0	-1.0	[]	44	
organization	OTHER	-1	-1.0	-1.0	[organistaion]	41	
title	STRING	&ndas,nd ro,	-1.0	-1.0	[]	62	
order	NUMBER	st [[,	-1.0	-1.0	[]	1	
name	STRING	,portr,leo b,.png,crop.,.jpg,-,	-1.0	-1.0	[image]	279	
buried	OTHER	march,janua,june,novem,	-1.0	-1.0	[burial]	42	
kohan	OTHER	march,octob,decem,july,febru,sheva,,,	-1.0	-1.0	[began]	67	
website	OTHER	-1	-1.0	-1.0	[]	80	
imagesize	OTHER	-1	-1.0	-1.0	[]	1	
caption	STRING	c eng,s, wh,for,nd fr,.,	-1.0	-1.0	[]	55	
suffix	OTHER	-1	-1.0	-1.0	[]	11	
deathplace	OTHER	may,janua,(appr,june,}},or,decem,(,,,augus,march,septe,octob,july,|,febru,novem,	-1.0	-1.0	[death_place, birthplace]	353	
nationality	OTHER	}}hun,	-1.0	-1.0	[]	94	
residence	OTHER	-1	-1.0	-1.0	[]	65	
other_post	OTHER	–,&ndas,	-1.0	-1.0	[]	21	
issue2	STRING	-1	-1.0	-1.0	[]	1	
issue3	OTHER	-1	-1.0	-1.0	[issue1]	3	
ethnicity	OTHER	-1	-1.0	-1.0	[]	1	
dynasty	OTHER	-1	-1.0	-1.0	[]	7	
rebbe	OTHER	-1	-1.0	-1.0	[]	2	

