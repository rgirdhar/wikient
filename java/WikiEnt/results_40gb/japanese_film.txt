studio	OTHER	°c]],	-1.0	-1.0	[]	63	
katakana	OTHER	-1	-1.0	-1.0	[]	1	
kanji	OTHER	ヒロシマの,お遍路大パ,	-1.0	-1.0	[ja_kanji, kana]	165	
budget	OTHER	milli,.,,,	-1.0	-1.0	[]	30	
gross	OTHER	milli,,,	-1.0	-1.0	[]	55	
runtime	NUMBER	min,min.,minut,&nbsp,mins,min (,	-1.0	-1.0	[]	105	
preceded_by	OTHER	: awa,]],]]'',	-1.0	-1.0	[]	20	
production	OTHER	-1	-1.0	-1.0	[]	6	
released	OTHER	{{fla,||}},}},in fi,decem,|,,,(scre,	-1.0	-1.0	[]	189	
story	OTHER	-1	-1.0	-1.0	[]	17	
title	STRING	-1	-1.0	-1.0	[]	1	
screenplay	OTHER	-1	-1.0	-1.0	[]	34	
image_size	NUMBER	px,	-1.0	-1.0	[]	1	
name	STRING	water,battl,: the,.,assas,	-1.0	-1.0	[]	189	
on	OTHER	-1	-1.0	-1.0	[by]	28	
distributor	OTHER	-1	-1.0	-1.0	[]	93	
cinematography	OTHER	-1	-1.0	-1.0	[]	81	
border	OTHER	-1	-1.0	-1.0	[]	0	
writer	OTHER	-1	-1.0	-1.0	[]	83	
revenue	OTHER	-1	-1.0	-1.0	[]	0	
music	OTHER	ug,	-1.0	-1.0	[]	104	
website	OTHER	-1	-1.0	-1.0	[]	1	
conductor	STRING	-1	-1.0	-1.0	[]	1	
narrator	OTHER	-1	-1.0	-1.0	[]	4	
editing	OTHER	-1	-1.0	-1.0	[]	63	
followed_by	OTHER	film),(anim,]],: leg,	-1.0	-1.0	[]	115	
image	OTHER	,c.jpg,.png,_japa,dvd c,.gif,.jpg,px-di,poste,	-1.0	-1.0	[]	88	
director	OTHER	-1	-1.0	-1.0	[artdirector]	212	
size	NUMBER	px,	-1.0	-1.0	[]	3	
country	STRING	-1	-1.0	-1.0	[]	178	
debuts	OTHER	-1	-1.0	-1.0	[]	4	
movie_music	OTHER	-1	-1.0	-1.0	[]	0	
latin	STRING	l.e.,) the,[[hd,),), ja,).,	-1.0	-1.0	[caption]	78	
awards	OTHER	-1	-1.0	-1.0	[]	1	
producer	OTHER	-1	-1.0	-1.0	[]	82	
romaji	STRING	: o-h,hiros,	-1.0	-1.0	[ja_romaji]	100	
language	OTHER	-1	-1.0	-1.0	[]	186	
based	OTHER	-1	-1.0	-1.0	[]	0	
starring	OTHER	)|mis,	-1.0	-1.0	[]	165	

