elevation_ref_point	STRING	-1	-1.0	-1.0	[]	25	
accessed_by	STRING	cable,	-1.0	-1.0	[]	18	
location	STRING	-1	-1.0	-1.0	[]	20	
bedspaces	OTHER	-1	-1.0	-1.0	[]	4	
elevation_footnotes	OTHER	-1	-1.0	-1.0	[]	0	
type	STRING	-1	-1.0	-1.0	[state, name]	77	
longs	NUMBER	, (em,, sle,	0.0	180.0	[longm, lats, bunks]	158	
camping	STRING	-1	-1.0	-1.0	[]	2	
admin_district	OTHER	-1	-1.0	-1.0	[]	15	
place	OTHER	-1	-1.0	-1.0	[]	13	
SAC	OTHER	-1	-1.0	-1.0	[PZS]	0	
image	STRING	).jpg,.jpg,	-1.0	-1.0	[]	27	
footnotes	OTHER	-1	-1.0	-1.0	[]	14	
country	STRING	-1	-1.0	-1.0	[]	39	
image_alt	STRING	-1	-1.0	-1.0	[]	1	
beds	OTHER	-1	-1.0	-1.0	[]	1	
national_park	STRING	-1	-1.0	-1.0	[]	11	
when_open	DATE	rd we,	-1.0	-1.0	[]	21	
owner	OTHER	-1	-1.0	-1.0	[]	25	
alternative_location_map	STRING	-1	-1.0	-1.0	[]	1	
AC_hut	STRING	-1	-1.0	-1.0	[]	26	
mattresses	NUMBER	-1	5.0	105.0	[]	23	
region	OTHER	-1	-1.0	-1.0	[design]	2	
pushpin_label_position	OTHER	-1	-1.0	-1.0	[]	0	
OeAV	NUMBER	-1	1.0	1309.0	[]	19	
winter_room	OTHER	beds,,	-1.0	-1.0	[]	14	
iso_region	STRING	-1	-1.0	-1.0	[]	40	
longEW	STRING	-1	-1.0	-1.0	[]	39	
built_by	OTHER	–,s,, ext,rebui,	-1.0	-1.0	[built]	34	
shakedowns	OTHER	-1	-1.0	-1.0	[]	3	
coordinates	OTHER	-1	-1.0	-1.0	[]	1	
mountain_range	OTHER	-1	-1.0	-1.0	[]	25	
water	STRING	-1	-1.0	-1.0	[latNS]	42	
built_for	STRING	-1	-1.0	-1.0	[]	13	
website	OTHER	hut w,	-1.0	-1.0	[]	20	
maintained_by	OTHER	-1	-1.0	-1.0	[]	11	
elevation	NUMBER	-1	829.0	2959.0	[]	26	
caption	OTHER	.,	-1.0	-1.0	[]	14	
accom_notes	OTHER	-1	-1.0	-1.0	[]	24	
material	OTHER	-1	-1.0	-1.0	[catering]	1	
TVN	NUMBER	-1	46.0	231861.0	[TK]	25	
AC_category	NUMBER	-1	1.0	2.0	[]	21	

