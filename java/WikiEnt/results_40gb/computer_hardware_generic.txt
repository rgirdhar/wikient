via3_2	STRING	-1	-1.0	-1.0	[via3_1]	2	
type	OTHER	-seri,-,	-1.0	-1.0	[date, name]	16	
power	NUMBER	w {{r,	-1.0	-1.0	[]	1	
processor	OTHER	micro,|pent,	-1.0	-1.0	[]	6	
connection	OTHER	-1	-1.0	-1.0	[]	0	
image	OTHER	ed.jp,ed.gi,.jpg,-ct,	-1.0	-1.0	[]	9	
class4	NUMBER	gbit/,mbit/,	-1.0	-1.0	[class2, class3]	3	
class1	OTHER	mbit/,	-1.0	-1.0	[]	2	
manufacturer	OTHER	-1	-1.0	-1.0	[]	7	
capacities	NUMBER	gb,,	-1.0	-1.0	[]	1	
conn3	STRING	-1	-1.0	-1.0	[conn2]	6	
weight	NUMBER	|lb|k,	-1.0	-1.0	[]	1	
designfirm	OTHER	-1	-1.0	-1.0	[]	0	
frequency	NUMBER	mhz -,mhz,.,-,	-1.0	-1.0	[]	5	
predecessor	NUMBER	]],	-1.0	-1.0	[]	1	
number_built	NUMBER	-1	75.0	75.0	[]	1	
dimensions	OTHER	x,	-1.0	-1.0	[]	5	
vial_9	OTHER	gen,|ibm,]],	-1.0	-1.0	[via2_8, via2_9, via2_3, via1_1, via2_4, via1_2, via2_5, via1_3, via2_6, via1_4, via1_5, via2_1, via1_6, via2_2, via1_7, via1_8, via1_9]	44	
caption	OTHER	serie,) [[p,x spe,s [[e,ed,(ct,	-1.0	-1.0	[]	11	
factor	NUMBER	[[mez,	-1.0	-1.0	[]	1	
memory	NUMBER	-bit],-,	-1.0	-1.0	[]	5	
via2_7	NUMBER	]],	-1.0	-1.0	[]	1	
ports	OTHER	decod,or,.,	-1.0	-1.0	[cost]	10	
introduced	OTHER	,,	-1.0	-1.0	[]	6	
discontinued	OTHER	-1	-1.0	-1.0	[]	2	
manuf6	OTHER	com]],	-1.0	-1.0	[manuf2, manuf5, manuf4, manuf3]	11	

