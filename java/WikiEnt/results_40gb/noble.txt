successor	OTHER	-1	-1.0	-1.0	[succession]	15	
profession	STRING	-1	-1.0	-1.0	[]	3	
issue	OTHER	, by,.png,siliq,.jpg,,,-,	-1.0	-1.0	[image]	102	
type	STRING	-1	-1.0	-1.0	[more, name]	192	
religion	OTHER	–,bc –,—,-,	-1.0	-1.0	[reign]	10	
predecessor	OTHER	-1	-1.0	-1.0	[predecesor]	7	
title	OTHER	–,nd [[,	-1.0	-1.0	[pipe]	69	
death_place	OTHER	april,june,decem,/,,,–,augus,march,septe,, {{d,octob,july,|,febru,bc,[[mex,	-1.0	-1.0	[death_date, birth_place]	248	
image_size	OTHER	px,	-1.0	-1.0	[]	34	
spouse	OTHER	–,)|hel,)|ann,),st ba,-,	-1.0	-1.0	[]	124	
alt	OTHER	px]],	-1.0	-1.0	[CoA]	105	
family	OTHER	-1	-1.0	-1.0	[]	136	
christening_place	OTHER	-1	-1.0	-1.0	[christening_date]	2	
occupation	OTHER	-,	-1.0	-1.0	[]	120	
birth_date	OTHER	augus,|,]],th ce,/,,,	-1.0	-1.0	[]	110	
styles	OTHER	-1	-1.0	-1.0	[]	1	
caption	STRING	.,-,	-1.0	-1.0	[]	31	
religious_beliefs	OTHER	-1	-1.0	-1.0	[]	1	
burial_place	OTHER	-1	-1.0	-1.0	[burial_date]	9	
nationality	OTHER	-1	-1.0	-1.0	[]	2	
mother	OTHER	–,)|mik,)|sta,)|hel,)|jan,;,)|ann,st ma,-,	-1.0	-1.0	[father]	258	
titles	STRING	-1	-1.0	-1.0	[]	8	
dynasty	STRING	-1	-1.0	-1.0	[]	1	

