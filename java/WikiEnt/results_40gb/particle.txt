baryon_number	NUMBER	|,	-1.0	-1.0	[]	1	
num_spin_states	OTHER	-1	-1.0	-1.0	[]	23	
X_charge	OTHER	-1	-1.0	-1.0	[]	2	
mass	OTHER	,|ul=m,[[ele,−,mev/c,{{now,|,.,[[mev,	-1.0	-1.0	[]	39	
theorised	OTHER	),	-1.0	-1.0	[]	2	
electric_polarizability	NUMBER	.,	-1.0	-1.0	[]	2	
composition	OTHER	[[up,}}: {,proto,s\bar,	-1.0	-1.0	[]	41	
num_types	OTHER	([[el,([[up,([[li,– ele,	-1.0	-1.0	[]	9	
generation	STRING	st,,	-1.0	-1.0	[]	17	
decay_particle	OTHER	}} +,%), [,.,/tabl,	-1.0	-1.0	[]	11	
electric_dipole_moment	NUMBER	.,	-1.0	-1.0	[]	2	
symbol	OTHER	}}, {,}},+ , h,}}, a,	-1.0	-1.0	[]	36	
status	STRING	,  an,	-1.0	-1.0	[]	11	
width	NUMBER	.,	-1.0	-1.0	[]	1	
image	NUMBER	.svg|,.jpg|,px|al,px]],.,	-1.0	-1.0	[]	18	
interaction	OTHER	-1	-1.0	-1.0	[]	35	
number	NUMBER	-1	0.0	-1.0	[]	1	
strangeness	OTHER	-1	-1.0	-1.0	[]	0	
isospin	NUMBER	|,	-1.0	-1.0	[]	4	
r_parity	OTHER	-1	-1.0	-1.0	[parity, c_parity, g_parity]	4	
magnetic_polarisability	OTHER	-1	-1.0	-1.0	[]	0	
decay_time	NUMBER	|e=-,.,	-1.0	-1.0	[]	5	
bgcolour	OTHER	-1	-1.0	-1.0	[]	0	
electric_polarisability	OTHER	-1	-1.0	-1.0	[]	0	
electric_charge	OTHER	|el=e,[[cou,|u=[[,[[ele,}}:,&nbsp,{{now,|ul=e,|,	-1.0	-1.0	[]	43	
radius	NUMBER	.,	-1.0	-1.0	[]	1	
theorized	NUMBER	–,) {{s,s,s  th,s),),, [[r,) [[g,)  [[,	-1.0	-1.0	[]	23	
L	OTHER	|,	-1.0	-1.0	[]	3	
color_charge	STRING	[[lin,	-1.0	-1.0	[]	17	
magnetic_moment	NUMBER	.,	-1.0	-1.0	[]	4	
statistics	OTHER	-1	-1.0	-1.0	[]	41	
magnetic_polarizability	NUMBER	.,	-1.0	-1.0	[]	2	
particle	OTHER	-1	-1.0	-1.0	[]	2	
condensed_symmetries	OTHER	|,	-1.0	-1.0	[]	6	
topness	NUMBER	-1	1.0	1.0	[]	1	
name	STRING	) bos,}} bo,	-1.0	-1.0	[]	44	
spin	OTHER	|,/,,,	-1.0	-1.0	[]	40	
weak_hypercharge	OTHER	|,	-1.0	-1.0	[]	12	
hypercharge	OTHER	-1	-1.0	-1.0	[]	0	
colour_charge	OTHER	-1	-1.0	-1.0	[]	0	
weak_isospin_3	OTHER	|,	-1.0	-1.0	[weak_isospin]	11	
lepton_number	OTHER	-1	-1.0	-1.0	[]	0	
mean_lifetime	OTHER	.,	-1.0	-1.0	[]	8	
chirality	OTHER	-1	-1.0	-1.0	[]	0	
caption	OTHER	mm le,meter,form,setup,.,,,	-1.0	-1.0	[]	19	
Grouping	OTHER	-1	-1.0	-1.0	[]	1	
charm	OTHER	-1	-1.0	-1.0	[]	0	
bottomness	OTHER	-1	-1.0	-1.0	[]	0	
antiparticle	OTHER	-1	-1.0	-1.0	[]	25	
charge_radius	NUMBER	.,	-1.0	-1.0	[]	1	
classification	OTHER	-1	-1.0	-1.0	[]	4	
group	OTHER	-1	-1.0	-1.0	[]	35	
discovered	NUMBER	–,) {{s,) and,) [[b,colla,july,]] an,) {{n,exper,),.,	1947.0	1968.0	[]	26	

