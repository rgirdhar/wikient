subcat	STRING	-1	-1.0	-1.0	[]	1509	
profession	OTHER	-1	-1.0	-1.0	[]	1	
influences	OTHER	-1	-1.0	-1.0	[influenced]	87	
deathdate	NUMBER	(aged,janua,may,or,decem,|||,,,(age,march,s,septe,|,or de,	1933.0	2009.0	[death_date]	506	
religion	OTHER	-1	-1.0	-1.0	[Religion]	3	
Influenced	STRING	-1	-1.0	-1.0	[]	1	
assistant	OTHER	-1	-1.0	-1.0	[]	1	
spouse	OTHER	–,child,june,-pres,–pres,-,	-1.0	-1.0	[Spouse]	23	
sortkey	SET_OF_STRINGS	rd,	-1.0	-1.0	[]	475	
movement	OTHER	-1	-1.0	-1.0	[]	4	
bodyclass	STRING	-1	-1.0	-1.0	[]	11	
place	OTHER	nd-co,,crop.,_-_p,a.jpg,s.jpg,salon,allre,tibe,_-_fa,- joe,doubl,jungf,_mich,jerry,) ret,(,bagno,s.png,.,san d,-,germa,orlan,..jpg,).jpe,).jpg,libra,_surb,) by,fnac,small,marve,- ben,- p,.png,- o t,.jpg,px.jp,- buf,)_by_,pic a,japan,) (cr,.gif,manga,.jpeg,_,tim g,-marv,sdcc.,apr,maine,-.jpg,shank,_fnac,cropp,abc.j,-riss,	-1.0	-1.0	[image]	1075	
signature	STRING	-1	-1.0	-1.0	[]	29	
affiliations	OTHER	, fou,	-1.0	-1.0	[]	2	
animation	STRING	-1	-1.0	-1.0	[]	2	
birth_date	NUMBER	}},june,decem,]],(age,aged,th,march,septe,; mar,july,janua,may,or,th ja,.,/,,,-,{{cit,||}},augus,|df=y,s,octob,|,th ju,in co,febru,novem,	1854.0	1987.0	[]	1857	
DESCRIPTION	STRING	-1	-1.0	-1.0	[]	1	
mob	OTHER	-1	-1.0	-1.0	[for]	14	
director	STRING	-1	-1.0	-1.0	[direct]	2	
Active	OTHER	-1	-1.0	-1.0	[]	0	
collaborators	OTHER	–,	-1.0	-1.0	[colaberators]	27	
nonAmerican	STRING	-1	-1.0	-1.0	[]	1	
bgcolour	NUMBER	ed,	-1.0	-1.0	[]	2	
acts	OTHER	-1	-1.0	-1.0	[]	1	
mater	OTHER	-1	-1.0	-1.0	[Mater, area]	869	
Background	OTHER	-1	-1.0	-1.0	[]	0	
publisher	STRING	-1	-1.0	-1.0	[publish]	100	
training	OTHER	-1	-1.0	-1.0	[]	3	
patrons	OTHER	-1	-1.0	-1.0	[parents]	0	
manga	STRING	-1	-1.0	-1.0	[]	111	
web	STRING	-1	-1.0	-1.0	[rss, ink]	439	
living	STRING	-1	-1.0	-1.0	[]	1	
deathplace	OTHER	}},|,	-1.0	-1.0	[birthplace, death_place]	1786	
works	OTHER	rober,s "wa,)'',[[hax,th mu,)|sup,[[har,[[nat,[[kod,(comi,) ivo,, [[e,) wcc,&,film),comic,[[ink,),.,[[int,/,,,-,[[gol,[[ang,|||ne,(oct.,|tang,) "ri,ways,''hul,fan a,: the,–,nd [[,th ko,), gr,'' '',yello,), go,[[sei,seeds,) "be,[[fri,''sup,th [[,(''[[,serie,ers]],-d wo,rd [[,tage,days,illus,"best,|eagl,[[joe,: ali,award,[[nor,doug,st [[,days',[[soc,prix,creat,'',, ''[,[[mil,speci,and,| ''",(shaz,proje,s car,, sil,|zarj,, dia,[[pul,[[spe,]]: e,[[wiz,[[h.l,russ,[[ign,) "fa,team),wizar,s),: a g,–pres,seven,great,[[emm,&ndas,, dou,gener,ad)|o,): th,ad)|s,vermo,]],xeric,ad)|t,), (,cover,[[kla,) (,nomin,);  p,: dra,urhun,: mis,), '',) elz,harve,salón,will,[[lis,years,in co,d hum,''  ',carto,(the,|elvi,th si,|||fo,)|mar,)  gi,silve,revam,[[eag,pages,web c,[[eis,ad (c,s)|at,) kal,), pl,[[ass,[[the,|vert,).,reboo,), "p,outst,''[[t,''[[r,''[[s,(maga,) ''[,{{awa,''[[e,''[[d,''[[c,"prem,postc,: "be,''[[j,[[mad,, mun,eagle,[[yal,)|sha,: bes,]] go,|''ac,[[wal,|kerr,[[reu,(best,) [[c,bulle,jack,shust,;  fi,juno,]]'',), co,|long,gosci,) [[n,) [[l,-ghos,) [[h,:''',natio,to a,), do,[[rus,[[sho,|[[bi,) nat,eisne,) wit,[[sha,)   [,	-1.0	-1.0	[awards]	2751	
debut_works	OTHER	-1	-1.0	-1.0	[]	0	
residence	STRING	-1	-1.0	-1.0	[]	1	
BIRTH_DATE	NUMBER	,,	-1.0	-1.0	[]	1	
notable_works	NUMBER	,,	-1.0	-1.0	[]	1	
design	STRING	-1	-1.0	-1.0	[]	1	
pencils	STRING	-1	-1.0	-1.0	[pencil, pencile]	614	
noted_works	OTHER	-1	-1.0	-1.0	[]	1	
genre	OTHER	-1	-1.0	-1.0	[]	2	
location	OTHER	,).'',[[alt,=|log,- oil,]],episo,event,), il,year,[[tra,) zen,#,(nov.,s [[t,comic,at a,[[san,(,),.,/,san d,,,-,signi,, rea,marve,novem,fan e,s pro,in bu,in [[,. gil,[[sma,in be,[[wor,. pho,[[won,(dec.,th [[,new y,[[scr,(febr,, hol,s,lucca,octob,).,taipe,st ma,), wi,, fro,[[mar,[[her,[[tor,"prem,, in,.  ph,, par,rd ju,th av,[[big,[[mus,, pho,'',acade,dalla,[[new,(pari,.]],gothe,editi,[[bro,anima,minne,[[mia,(jan.,ink p,texas,super,in fr,|janu,in no,for t,calga,film,with,, wit,at [[,s.,at th,canne,)  dc,[[com,): "t,in or,[[rue,	-1.0	-1.0	[caption, education]	880	
nationality2	STRING	-1	-1.0	-1.0	[]	4	
twitter	OTHER	-1	-1.0	-1.0	[title]	4	
cause_death	OTHER	-1	-1.0	-1.0	[]	0	
description	NUMBER	=tony,	-1.0	-1.0	[]	1	
writer	STRING	),. oil,/,	-1.0	-1.0	[editor, write, painter, letter]	1482	
occupation	OTHER	-1	-1.0	-1.0	[]	8	
boxwidth	NUMBER	em,	-1.0	-1.0	[]	1	
strip	STRING	-1	-1.0	-1.0	[]	20	
period	NUMBER	to th,	-1.0	-1.0	[]	2	
Movement	STRING	-1	-1.0	-1.0	[]	1	
alma_mater	OTHER	-1	-1.0	-1.0	[]	9	
country	OTHER	-1	-1.0	-1.0	[]	1	
size	NUMBER	px,|,,,th,	125.0	125.0	[date]	11	
subcat2	OTHER	-1	-1.0	-1.0	[]	7	
Img_size	OTHER	-1	-1.0	-1.0	[]	0	
relatives	OTHER	-1	-1.0	-1.0	[]	1	
birthname	STRING	}},octob,decem,|,,,	-1.0	-1.0	[birth_name]	842	
signature_alt	STRING	-1	-1.0	-1.0	[]	5	
children	OTHER	),	-1.0	-1.0	[]	5	
name_nonEN	OTHER	-1	-1.0	-1.0	[]	32	
noimage	STRING	-1	-1.0	-1.0	[]	3	
name	STRING	|天野 こ,|荒川 弘,|上北ふた,|森薫}},|東 まゆ,|青山 剛,|鎌谷 悠,|岡崎 能,|八木 教,|かずと,|星野 桂,	-1.0	-1.0	[]	1208	
artist	STRING	-1	-1.0	-1.0	[]	8	
website	OTHER	/inde,.fc,.ocn.,.html,.com/,.devi,ozcom,th.co,/,-,s%,th-tu,?ref=,orsi.,/mang,.trip,.plal,ep.co,.com,studi,twent,	-1.0	-1.0	[]	818	
alias	STRING	micha,secre,-,	-1.0	-1.0	[]	334	
imagesize	NUMBER	-1	0.65	40000.0	[image_size]	611	
birth_year	NUMBER	-1	1981.0	1981.0	[]	1	
nonUS	STRING	-1	-1.0	-1.0	[]	159	
pseudonym	STRING	-1	-1.0	-1.0	[]	1	
cartoonist	STRING	-1	-1.0	-1.0	[cartoon]	369	
nationality	OTHER	px|]],px]],px|ri,	-1.0	-1.0	[Nationality]	2234	
color	STRING	-1	-1.0	-1.0	[]	157	
Img_capt	OTHER	-1	-1.0	-1.0	[]	0	
yod	NUMBER	-1	3.0	2004.0	[yob]	4	
yearsactive	NUMBER	–,]]-pr,–pres,	-1.0	-1.0	[years_active]	6	

