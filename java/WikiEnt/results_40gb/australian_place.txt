president	STRING	-1	-1.0	-1.0	[]	2	
altitude	NUMBER	-,	-1.0	-1.0	[]	1	
mintemp	NUMBER	-1	-18.0	900.0	[maxtemp]	1435	
elevation_footnotes	OTHER	}},.shtm,-,	-1.0	-1.0	[]	11	
area_footnotes	NUMBER	&,}},:,|a}},	-1.0	-1.0	[]	24	
longs	NUMBER	-1	-23.341	153.273	[longm, lats]	17691	
est	NUMBER	(dist,,, ren,(as t,(appr,}},'s (a,june,&nbsp,(expl,s (ga,(plan,decem,(as m,shire,as co,march,septe,(loca,{{ver,july,gazet,{{ref,(town,janua,may,(land,(rena,april,s - e,to th,s, (l,/,,,-,in au,(gaze,augus,(as c,s,'s,octob,, sep,febru,th ce,kilom,- as,	0.0	4262.0	[dist5]	13247	
deputy	STRING	-1	-1.0	-1.0	[]	1	
poprank	NUMBER	nd,st,rd,th,	29.0	42.0	[]	33	
stategov6	OTHER	)'',elect,other,%),),divis,	-1.0	-1.0	[stategov5]	6918	
postcode	NUMBER	,(will,(the,]],(and,,,and,-,	800.0	8436.0	[Postcode]	6642	
location5	OTHER	.svg|,}},km. f,kms f,km no,.,	-1.0	-1.0	[location4]	11621	
ward	OTHER	bagda,px]],	-1.0	-1.0	[w, url]	5017	
county	OTHER	-1	-1.0	-1.0	[city]	4046	
alternative_location_map	STRING	-1	-1.0	-1.0	[]	793	
region	OTHER	-1	-1.0	-1.0	[]	892	
mintemp_footnotes	OTHER	-1	-1.0	-1.0	[maxtemp_footnotes]	0	
fedgov5	OTHER	)'',redis,other,%),.gif|,),.png|,divis,	-1.0	-1.0	[fedgov4]	6877	
dir5	STRING	-1	-1.0	-1.0	[dir4]	7005	
logosize	NUMBER	px,	50.0	270.0	[]	223	
latNS	STRING	-1	-1.0	-1.0	[]	17	
mayor	STRING	}},),-,	-1.0	-1.0	[]	570	
rainfall	NUMBER	}},-,	0.0	4200.0	[]	775	
elevation	NUMBER	-1	0.0	1640.0	[]	1400	
attractions	STRING	-1	-1.0	-1.0	[]	1	
utc	OTHER	,(dist,}}&nb,(as o,}},(regi,(post,&slac,cr ch,]],|aest,in th,(cens,}} (,(resi,estim,aroun,(loca,([[ce,munic,e&col,''(,inclu,(town,censu,'',{{cen,+,(,(est),),.,/,(esti,,,(with,-,(not,(est.,(up t,{{cit,perm,,([[ou,:,(py m,	-1.0	-1.0	[pop, sw]	34788	
force_national_map	STRING	-1	-1.0	-1.0	[]	12	
pushpin_map_caption	STRING	-1	-1.0	-1.0	[]	11	
mayortitle	OTHER	-1	-1.0	-1.0	[]	18	
type	STRING	mine,x,	-1.0	-1.0	[Type, state, name]	23430	
popfootnotes	NUMBER	) {{c,censu,aus |,}},est),sd),) abs,),.,abs),estim,aus,)  ([,aus|i,july,=,large,	557.0	557.0	[footnotes, pop_footnotes]	1134	
timezone	OTHER	|awst,:,|aest,	-1.0	-1.0	[]	280	
parish	STRING	-1	-1.0	-1.0	[]	502	
seet	OTHER	hughe,king,	-1.0	-1.0	[seat]	9834	
imagesize2	NUMBER	px,	100.0	300.0	[image2size, imagesize]	535	
country	STRING	-1	-1.0	-1.0	[council]	2	
coordinates_type	NUMBER	_regi,	-1.0	-1.0	[]	1	
near	STRING	-1	-1.0	-1.0	[]	1	
Population	NUMBER	-1	1109.0	1109.0	[]	1	
abolished	NUMBER	-1	1949.0	2011.0	[]	6	
gazetted	NUMBER	may,march,augus,septe,octob,	-1.0	-1.0	[]	31	
density	NUMBER	-1	0.001	16493.3	[]	282	
use_lga_map	STRING	-1	-1.0	-1.0	[]	890	
pushpin_label_position	STRING	-1	-1.0	-1.0	[]	1931	
rainfall_footnotes	OTHER	-1	-1.0	-1.0	[]	1	
caption2	STRING	, ren,km to,) the,house,(when,|m|ft,in th,.svg|,level,, is,at sy,. cor,.  th,/ cai,]] tr,is it,s, wi,), bo,(aust,hours,[[mer,steps,),and n,.,]] wi,,,- fil,-,|km|m,), ga,flood,, as,years,febru,th-ce,dower,. car,sprin,, nea,}},rd st,s and,-degr,ft le,minin,. a r,dec,henry,s sho,m abo,s (be,murra,park,, on,relea,s),; fro,photo,metre,bar &,|acre,s,easte,s sil,but t,th ce,	-1.0	-1.0	[caption]	3996	
density_footnotes	NUMBER	}},),.,abs e,	18.0	18.0	[]	21	
area	NUMBER	.,	0.1	624339.0	[]	2223	
longEW	STRING	.svg,.png,.jpg,.gif,	-1.0	-1.0	[logo]	363	
_noautocat	STRING	-1	-1.0	-1.0	[]	206	
coordinates_display	OTHER	|,	-1.0	-1.0	[]	2	
coordindates	NUMBER	|s|,°,|,.,	-1.0	-1.0	[coordinates]	2128	
website	OTHER	-1	-1.0	-1.0	[]	1	
image2	STRING	- nov,,_eryl,_vict,-_,a.jpg,).png,s.jpg,lg.jp,victo,inker,b-iro,level,avalo,at sy,_hart,)brae,wathe,nsw,,_brok,)_pub,c.jpg,,_.jp,_map_,-town,)seve,samfo,lga.g,)ku-r,),.,on th,,,-,sale,s evp,oct,wefre,).jpg,)bent,edite,-wiki,races,w.jpg,bondi,smc.j,castl,.png,sprin,cape,-web.,.jpg,_brib,_lind,baird,)_blu,sceal,b-web,-jun-,aeria,_dace,_volc,.jpeg,mine,_gobe,park.,, roa,may,)_mai,)_the,)darl,_smc.,_feet,gnang,_weym,_sout,gobei,sydne,weymo,s,r,tract,kelbu,aug,plaza,sugar,venus,	-1.0	-1.0	[image]	4355	
Government	STRING	-1	-1.0	-1.0	[]	1	
popjhgyufffffu_footnotes	OTHER	-1	-1.0	-1.0	[]	0	

