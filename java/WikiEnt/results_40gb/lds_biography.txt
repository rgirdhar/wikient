president	STRING	-1	-1.0	-1.0	[]	1	
political_office2	STRING	-1	-1.0	-1.0	[]	1	
party	OTHER	-1	-1.0	-1.0	[]	2	
list_notes	OTHER	-1	-1.0	-1.0	[]	3	
political_office1	OTHER	th pr,	-1.0	-1.0	[]	7	
rank	STRING	byu c,	-1.0	-1.0	[alt, name]	324	
image_size	NUMBER	px,	-1.0	-1.0	[]	56	
spouse	STRING	–,-pres,–pres,-,	-1.0	-1.0	[]	37	
military_awards	STRING	-1	-1.0	-1.0	[]	1	
signature	STRING	-1	-1.0	-1.0	[]	2	
ordination_reason3	STRING	-1	-1.0	-1.0	[ordination_reason2]	5	
home_town	STRING	-1	-1.0	-1.0	[]	1	
nickname	STRING	-1	-1.0	-1.0	[]	1	
start_date6	NUMBER	(aged,|,,,	1842.0	1934.0	[start_date4, start_date5]	443	
image	STRING	.jpg,	-1.0	-1.0	[PD_image]	103	
monuments	STRING	-1	-1.0	-1.0	[]	1	
branch	OTHER	-1	-1.0	-1.0	[]	2	
years	OTHER	-1	-1.0	-1.0	[]	1	
allegiance	STRING	-1	-1.0	-1.0	[]	1	
title	STRING	-1	-1.0	-1.0	[]	1	
end_date6	NUMBER	(aged,|,,,	1844.0	1847.0	[end_date5]	309	
term_end2	OTHER	,,	-1.0	-1.0	[term_end1]	5	
caption	OTHER	comme,(age,	-1.0	-1.0	[]	11	
term_start1	OTHER	,,	-1.0	-1.0	[]	7	
residence	OTHER	-1	-1.0	-1.0	[president6]	440	
term_start2	NUMBER	-1	1884.0	1884.0	[]	1	
notable_works	OTHER	mqaaa,	-1.0	-1.0	[]	2	
battles_label	STRING	-1	-1.0	-1.0	[]	1	
serviceyears	OTHER	-,	-1.0	-1.0	[]	2	
birth_name	STRING	-1	-1.0	-1.0	[]	210	
called_by2	OTHER	-1	-1.0	-1.0	[called_by1]	3	
end_reason6	STRING	-1	-1.0	-1.0	[end_reason3, end_reason5, end_reason4]	256	
death_place	OTHER	-1	-1.0	-1.0	[birth_place]	365	
death_date	NUMBER	(aged,or,|,,,	1785.0	1873.0	[birth_date]	367	
occupation	OTHER	-1	-1.0	-1.0	[]	3	
alma_mater	OTHER	-1	-1.0	-1.0	[]	5	
footnotes	STRING	-1	-1.0	-1.0	[]	1	
size	OTHER	-1	-1.0	-1.0	[]	0	
resting_place	OTHER	dcf,	-1.0	-1.0	[]	13	
death_cause	STRING	-1	-1.0	-1.0	[]	1	
predecessor3	OTHER	-1	-1.0	-1.0	[predecessor2]	8	
office_successor1	OTHER	-1	-1.0	-1.0	[]	4	
awards	STRING	-1	-1.0	-1.0	[]	1	
relatives	STRING	-1	-1.0	-1.0	[]	1	
battles	STRING	-1	-1.0	-1.0	[]	1	
signature_alt	STRING	-1	-1.0	-1.0	[]	2	
resting_place_coordinates	NUMBER	|,.,	-1.0	-1.0	[]	12	
parents	STRING	-1	-1.0	-1.0	[]	20	
commands	STRING	-1	-1.0	-1.0	[]	1	
office_predecessor1	OTHER	-1	-1.0	-1.0	[]	3	
employer	OTHER	-1	-1.0	-1.0	[]	2	
children	OTHER	to,(,, inc,	-1.0	-1.0	[]	25	
education	STRING	-1	-1.0	-1.0	[]	1	
signature_size	NUMBER	px,	-1.0	-1.0	[]	1	
position_or_quorum6	OTHER	th ge,nd ge,st se,	-1.0	-1.0	[position_or_quorum5]	443	
website	OTHER	-1	-1.0	-1.0	[]	2	
reorganization2	STRING	-1	-1.0	-1.0	[organization, reorganization1]	4	
reorganization3	OTHER	-1	-1.0	-1.0	[]	0	
unit	STRING	-1	-1.0	-1.0	[]	1	
nationality	STRING	-1	-1.0	-1.0	[]	4	
servicenumber	STRING	-1	-1.0	-1.0	[]	1	
successor3	OTHER	),	-1.0	-1.0	[successor2]	7	

