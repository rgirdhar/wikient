qualitycharacteristictype	STRING	-1	-1.0	-1.0	[]	9	
sizeofshift	NUMBER	.,	-1.0	-1.0	[]	9	
varcenter	NUMBER	}^m \,}^m m,	-1.0	-1.0	[]	3	
proposer	OTHER	-1	-1.0	-1.0	[]	9	
measurementtype	STRING	-1	-1.0	-1.0	[]	9	
distribution	OTHER	-1	-1.0	-1.0	[]	9	
meancenter	OTHER	}^m x,}^m \,	-1.0	-1.0	[]	9	
subgroupsize	STRING	< n ≤,	-1.0	-1.0	[]	9	
varupperlimit	NUMBER	, x_i,\bar,, \le,\over,	-1.0	-1.0	[varlowerlimit, meanupperlimit]	8	
varchart	STRING	-1	-1.0	-1.0	[meanchart]	10	
varstatistic	OTHER	}^n \,	-1.0	-1.0	[]	3	
name	OTHER	-1	-1.0	-1.0	[arl]	8	
meanlimits	NUMBER	\bar,- \la,\sqrt,\frac,	-1.0	-1.0	[]	8	
meanstatistic	NUMBER	}^i \,- \la,}^n \,}^n x,	-1.0	-1.0	[]	10	

