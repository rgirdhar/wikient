location	OTHER	w., [,km nw,km) s,km no,),miles,|,.,½ mil,]], [,	-1.0	-1.0	[]	680	
locator_position	STRING	-1	-1.0	-1.0	[]	1	
longd	NUMBER	-1	-121.56063	167.1671567	[lat_d, long_m, long_d, latd, long_s]	177	
lat_s	NUMBER	-1	0.0	59.0	[lat_m]	61	
flow	NUMBER	,,	-1.0	-1.0	[]	1	
type	STRING	-tier,wonde,rapid,	-1.0	-1.0	[name, map]	1280	
label_position	STRING	-1	-1.0	-1.0	[]	1	
height	NUMBER	m (,feet/,&nbsp,|m|ft,m /,meter,ft /,ft,ft (,|m|ab,|ft|m,or,feet,m,.,,,|m}},|-|,-,|ft|,metre,ft.,|to|,- {{c,|ft}},|ft|a,|m|,	18.0	150.0	[]	606	
longs	OTHER	-1	-1.0	-1.0	[longm, lats]	0	
long_EW	STRING	-1	-1.0	-1.0	[longEW]	42	
height_longest	NUMBER	|m|ab,|ft|m,feet/,m (,feet,m,.,|m|ft,m /,,,-,|m}},metre,meter,|ft|a,	-1.0	-1.0	[]	113	
coords_ref	NUMBER	.,	-1.0	-1.0	[]	1	
Weather	STRING	-1	-1.0	-1.0	[]	1	
map_label	STRING	-1	-1.0	-1.0	[]	6	
nickname	STRING	-1	-1.0	-1.0	[]	1	
map_caption	STRING	-1	-1.0	-1.0	[]	10	
topo	OTHER	,janua,.png,a.jpg,edit.,.jpg,px.jp,-the,shira,px-,-,_trip,pics,).jpg,falls,mg,_wann,silve,_nigr,copy,, bos,	-1.0	-1.0	[drop, photo]	467	
width	NUMBER	–,|m|ab,meter,|ft|m,m and,m (,|ft|a,feet,m,.,|m|ft,,,	-1.0	-1.0	[]	166	
image_map	OTHER	-1	-1.0	-1.0	[]	0	
format	STRING	-1	-1.0	-1.0	[for]	2	
ascent	OTHER	-1	-1.0	-1.0	[]	1	
embedded	OTHER	-1	-1.0	-1.0	[]	1	
depth	OTHER	-1	-1.0	-1.0	[]	0	
map_label_position	STRING	-1	-1.0	-1.0	[]	1	
size	OTHER	-1	-1.0	-1.0	[age]	0	
numberofdrops	NUMBER	or mo,total,with,, wit,(one,-,	1.0	275.0	[number_drops]	298	
District	OTHER	-1	-1.0	-1.0	[]	1	
map_width	NUMBER	n,	200.0	230.0	[]	6	
region	STRING	-1	-1.0	-1.0	[]	25	
scale	NUMBER	-1	30000.0	30000.0	[]	1	
pushpin_map_alt	OTHER	-1	-1.0	-1.0	[]	0	
photo_alt	STRING	-1	-1.0	-1.0	[]	1	
coordinates_ref	OTHER	-1	-1.0	-1.0	[]	0	
pushpin_label_position	STRING	-1	-1.0	-1.0	[]	1	
last_eruption	OTHER	-1	-1.0	-1.0	[]	0	
Maximum_Recorded_flow	NUMBER	,,	-1.0	-1.0	[]	1	
relief	NUMBER	-1	1.0	1.0	[]	4	
distance	NUMBER	feet,	-1.0	-1.0	[]	1	
easiest_route	OTHER	-1	-1.0	-1.0	[]	1	
grid_ref_UK	STRING	-1	-1.0	-1.0	[]	2	
lat_NS	STRING	-1	-1.0	-1.0	[latNS]	42	
coordinates_display	SET_OF_STRINGS	-1	-1.0	-1.0	[]	1	
Run	NUMBER	ft (,	-1.0	-1.0	[]	1	
discharge_average	NUMBER	-1	730.0	730.0	[]	1	
world_rank	OTHER	{{cit,th,, ''',	-1.0	-1.0	[]	501	
photo_caption	STRING	ft dr,% of,|ft|m,, fou,, the,durin,july,paint,- a p,),.,	-1.0	-1.0	[]	416	
coords_type	STRING	-1	-1.0	-1.0	[]	5	
pushpin_map	STRING	-1	-1.0	-1.0	[]	1	
alt_name	OTHER	-1	-1.0	-1.0	[]	28	
elevation	NUMBER	|m|ab,,|ft|m,feet,m,.,|m|ft,,,|m}},|ft|,ft.,metre,|ft}},|ft|a,|m|,ft (,	4380.0	4380.0	[]	76	
prominence	OTHER	-1	-1.0	-1.0	[]	0	
average_flow	NUMBER	litre,m³/s,&nbsp,|cuft,cu ft,cubic,|m,m³ (,m,.,,,|ft,	-1.0	-1.0	[]	49	
average_width	NUMBER	|m|ab,|ft|m,feet/,feet,m,.,|m|ft,,,|m}},meter,|ft}},ft,ft (,	-1.0	-1.0	[]	49	
watersource	OTHER	-1	-1.0	-1.0	[watercourse]	447	
entrances	OTHER	-1	-1.0	-1.0	[]	0	
coords	NUMBER	|,.,	-1.0	-1.0	[]	369	
pushpin_map_caption	STRING	-1	-1.0	-1.0	[]	1	
photo_width	NUMBER	px,	4.0	350.0	[]	73	
source	STRING	-1	-1.0	-1.0	[]	5	
average_flow_rate	OTHER	m³/s,	-1.0	-1.0	[]	6	
listing	OTHER	-1	-1.0	-1.0	[]	0	
map_alt	OTHER	-1	-1.0	-1.0	[]	0	
native_name	OTHER	-1	-1.0	-1.0	[]	1	
drops	NUMBER	-1	1.0	1.0	[]	1	

