position	NUMBER	st {{,st  (,nd (,pts),(,rd,th,rd  (,nd  (,th (,nd,st (,st,th  (,rd (,	0.0	34.0	[]	219	
PRONUNCIATION	SET_OF_STRINGS	-1	-1.0	-1.0	[]	1	
nicknames	STRING	-1	-1.0	-1.0	[Nicknames]	14	
race	NUMBER	}} [[,}},}} mo,cc [[,spani,czech,europ,frenc,briti,valen,catal,isle,japan,grand,south,baden,argen,	0.0	117.0	[Place, laps]	1056	
Poles	NUMBER	-1	0.0	58.0	[]	639	
statistics	STRING	-1	-1.0	-1.0	[]	1	
Birthplace	OTHER	-1	-1.0	-1.0	[]	1	
death_place	OTHER	-1	-1.0	-1.0	[birth_place]	582	
Podiums	NUMBER	}},	0.0	288.0	[]	725	
image_size	NUMBER	px,	-1.0	-1.0	[]	1	
family	STRING	-1	-1.0	-1.0	[]	1	
name	STRING	-1	-1.0	-1.0	[]	957	
Birthdate	NUMBER	|,	-1.0	-1.0	[]	2	
points	NUMBER	(,,,	0.0	2349.0	[Wins]	1322	
death_date	NUMBER	(aged,may,janua,april,}},,,augus,march,septe,octob,{{dea,july,|,febru,novem,	1880.0	1971.0	[birth_date]	626	
NAME	STRING	-1	-1.0	-1.0	[]	1	
Website	NUMBER	.com.,	-1.0	-1.0	[]	1	
website	OTHER	racin,.html,.com/,.com,.es s,.com],	-1.0	-1.0	[]	401	
birth_death	OTHER	-1	-1.0	-1.0	[]	1	
image	OTHER	,hirom,-b,podiu,img,cregn,round,cm³,shini,senio,-e.jp,suzuk,sbk d,dayto,qatar,sbk s,-,valen,brno,sepan,cente,oct.j,start,nãœrb,th.jp,misan,assen,.jpg,motog,takum,japan,mugel,phill,moto,.jpeg,india,silve,estor,ssp d,donin,-ruph,. áng,brayh,javie,reviv,ssp p,marti,londo,_esto,tt,shuhe,moteg,	-1.0	-1.0	[]	298	
caption	OTHER	[[mac,podiu,&nbsp,malay,[[day,senio,champ,dutch,dayto,san m,czech,(,qatar,.,,,germa,mfj s,cente,[[bri,bsb a,austr,[[jap,bsb c,cc su,]].,formu,v]] i,june,cc ra,arago,super,japan,india,on a,silve,donin,[[eif,at cr,at th,frenc,in,briti,itali,on hi,v,catal,isle,[[suz,portu,).,	-1.0	-1.0	[]	254	
number	NUMBER	(form,([[ho,	1.0	222.0	[]	291	
win	NUMBER	,}} [[,}},natio,swedi,cc [[,czech,briti,itali,catal,isle,japan,grand,ultra,	-1.0	-1.0	[]	302	
Manufacturers	OTHER	f,- yam,}}) ,,)  {{,]], [,-,–,}}-{{,]]-ho,}}–{{,}}),}}) [,) [[h,	-1.0	-1.0	[]	198	
team	OTHER	|grup,oils,v|kr,corse,tuent,hp tu,]] ra,]],,,ciatt,|mons,roc|e,mo bo,]] [[,c har,	-1.0	-1.0	[Teams]	596	
Speed	NUMBER	.,	-1.0	-1.0	[]	1	
nationality	OTHER	}} [[,}}n,}},	-1.0	-1.0	[]	955	
Contested	NUMBER	([[,	4.0	4.0	[]	88	
season	NUMBER	-1	1981.0	2012.0	[]	221	
Championships	OTHER	({{mg,cc-[[,cc: [,cc –,&nbsp,cc- [,),. {{s,]] -,cc &n,({{wo,}}, {,) [[,cc -,grand,- [[f,([[,({{sb,	-1.0	-1.0	[]	403	
years	NUMBER	- pre,}},-[[,briti,– pre,,,–pres,-,{{-}},}}-{{,–,super,}}, {,}}-,}} -,grand,}}–{{,}}–,}}–pr,	0.0	2010.0	[Starts]	1270	
ethnicity	OTHER	-1	-1.0	-1.0	[]	1	

