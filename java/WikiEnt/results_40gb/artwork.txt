Condition	OTHER	-1	-1.0	-1.0	[]	1	
Catalogue	NUMBER	cc,	-1.0	-1.0	[]	1	
birth_name	OTHER	-1	-1.0	-1.0	[]	0	
location	OTHER	'',),=whit,.,).,– pre,th av,,,	-1.0	-1.0	[caption]	56	
length_inch	NUMBER	-1	5.0	276.0	[]	18	
locmapin	STRING	-1	-1.0	-1.0	[]	2	
nrhp_type	OTHER	-1	-1.0	-1.0	[]	0	
type	OTHER	bronz,',feet,plaqu,% [[f,),	-1.0	-1.0	[]	1009	
width_metric	NUMBER	,,-,x,	1.0	4800.0	[]	239	
diameter_cm	OTHER	as in,	-1.0	-1.0	[]	2	
height_inch	NUMBER	,¾,',½,{{fra,|,	2.0	300.0	[]	57	
backcolor	NUMBER	df,	-1.0	-1.0	[]	20	
city	OTHER	w. wi,rd st,th an,w. wa,th st,north,w. ma,west,	-1.0	-1.0	[]	1009	
Affiliation	OTHER	-1	-1.0	-1.0	[]	1	
death_place	OTHER	-1	-1.0	-1.0	[death_date]	1	
diameter_metric	NUMBER	-1	0.4	58464.0	[]	11	
longs	NUMBER	-1	0.0	140.0132713	[longm, lats]	21	
year	OTHER	to,&ndas,, ren,}}—{{,]],or c.,}}-,rd mi,}}–{{,and c,bc,}}–,(comm,; ded,and t,),/,and,-,}} –,(inst,|df=y,: cre,(inau,(firs,th–,s}},}}-pr,}}, f,}},nd ce,}}, d,}}&nd,, ded,–,}}-{{,}} -,—,(usa),}}{{n,}} (s,; rec,bce,in ar,s,bronz,~,: ori,|,th ce,rd ce,	-1.0	-1.0	[e_1, area]	1161	
longitud	NUMBER	-1	255.0	255.0	[]	1	
diameter_inch	NUMBER	as in,	-1.0	-1.0	[]	1	
credit	NUMBER	}},	-1.0	-1.0	[]	1	
sculptor	OTHER	-1	-1.0	-1.0	[]	1	
work	STRING	-1	-1.0	-1.0	[]	1	
geo	NUMBER	px,|,	-1.0	-1.0	[coor, dim]	4	
church	OTHER	-1	-1.0	-1.0	[]	1	
birth_date	NUMBER	|,	-1.0	-1.0	[]	1	
metric_unit	STRING	-1	-1.0	-1.0	[]	552	
diameter__metric	OTHER	-1	-1.0	-1.0	[]	0	
condition	STRING	-1	-1.0	-1.0	[]	1	
fabricator	STRING	-1	-1.0	-1.0	[]	1	
width	NUMBER	',.,	17.0	15850.0	[]	122	
image	NUMBER	px|gi,.jpg|,px|me,, mus,px]],px|th,px|bu,),px|to,,,-,px|st,- geo,circa,px|ca,	-1.0	-1.0	[date]	45	
image_caption	STRING	-1	-1.0	-1.0	[]	1	
other_language_2	STRING	-1	-1.0	-1.0	[other_language_1]	83	
museum	OTHER	.}},]] [[,]],]], [,	-1.0	-1.0	[]	676	
birth_place	SET_OF_STRINGS	-1	-1.0	-1.0	[]	1	
coordinates_type	STRING	-1	-1.0	-1.0	[]	1	
height_metric	NUMBER	meter,×,.,,,	1.67	3650.0	[]	261	
bgcolour	OTHER	-1	-1.0	-1.0	[]	0	
owner	OTHER	b]],,),.,west,	-1.0	-1.0	[]	830	
image_file	OTHER	,to,c_,v (cr,no,rmf_r,. óle,in lo,fairm,) con,secon,sq ft,) a.j,, mus,b.jpg,kano.,contr,.,rmf r,- the,,,lo-re,-,).jpg,antoi,.png,pm .j,.jpg,chaga,zweig,. oil,–,px-th,ary s,phill,.jpeg,_,-barc,dubef,youca,cr.jp,front,) (co,bild-,prope,detro,portr,--,s,) sos,-- la,x,	-1.0	-1.0	[]	1002	
image_alignment	STRING	-1	-1.0	-1.0	[]	2	
Period	OTHER	-1	-1.0	-1.0	[]	1	
diameter_imperial	NUMBER	feet,	1.5	464654.0	[]	10	
weight	NUMBER	',|kg|l,,,	2.1	8530.0	[height]	133	
other_title_2	OTHER	° la,	-1.0	-1.0	[]	9	
other_title_1	STRING	eiche,the w,), ma,||蛸と海,july,	-1.0	-1.0	[]	89	
subject	OTHER	]] lo,	-1.0	-1.0	[]	6	
data	NUMBER	]],	-1.0	-1.0	[]	1	
width_inch	NUMBER	,¾,as in,{{fra,|,	4.0	216.0	[]	46	
iso_region	STRING	-1	-1.0	-1.0	[]	2	
title	STRING	a),b,th re,oaks,. oil,/,-,, fro,baske,octob,pm,years,st re,th av,juill,secon,: gle,	-1.0	-1.0	[]	1177	
longEW	STRING	-1	-1.0	-1.0	[]	3	
width_imperial	NUMBER	–,",,ft.,¾,¼,inche,feet,|,.,-,	0.5	2160.0	[]	387	
name	STRING	- gal,attac,	-1.0	-1.0	[alt]	115	
dimensions	NUMBER	,|m|ab,|ft|,|ft|m,cm,',&nbsp,|x|,|ft|c,|cm|i,.,|m|ft,	-1.0	-1.0	[]	19	
coordinates_display	STRING	-1	-1.0	-1.0	[]	2	
artist	OTHER	&ndas,),anony,-,	-1.0	-1.0	[]	1123	
coordinates	NUMBER	|,.,/,	38.91211	38.91211	[]	407	
latNS	STRING	-1	-1.0	-1.0	[]	3	
imperial_unit	STRING	-1	-1.0	-1.0	[]	541	
pushpin_map	STRING	-1	-1.0	-1.0	[]	4	
designer	STRING	-1	-1.0	-1.0	[]	1	
anchura	NUMBER	-1	154.0	154.0	[]	1	
material	OTHER	-1	-1.0	-1.0	[]	50	
map_size	NUMBER	px,.,	152.0	250.0	[imagesize]	986	
field	OTHER	-1	-1.0	-1.0	[]	0	
painting_alignment	STRING	-1	-1.0	-1.0	[]	469	
pushpin_map_caption	STRING	-1	-1.0	-1.0	[]	2	
pushpin_mapsize	NUMBER	-1	50.0	260.0	[]	2	
length_imperial	NUMBER	–,,ft.,inche,feet,.,in,	0.5	12134.0	[]	291	
length_metric	NUMBER	',	6.9	1583.0	[]	31	
height_imperial	NUMBER	,to,inche,feet,(,.," to,in,–,ft.,¼,½,on a,ft,|,	0.75	1437.0	[]	471	

