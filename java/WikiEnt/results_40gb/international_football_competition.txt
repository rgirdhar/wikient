tourney_name	OTHER	tourn,cup,algar,afc b,consu,nehru,queen,footb,sea g,uefa,singa,europ,fifa,all-a,caf b,asian,afc u,quali,champ,gulf,th in,uniff,east,firs,th na,st ga,conca,youth,),women,ofc b,waff,-a-si,copa,men's,south,afc a,olymp,world,kirin,	-1.0	-1.0	[]	1511	
Other_titles	NUMBER	afc w,	-1.0	-1.0	[]	1	
count	NUMBER	nd,),	1.0	19.0	[club]	1423	
second	STRING	}} [[,	-1.0	-1.0	[]	636	
player	OTHER	}} [[,)|max,)|pak,	-1.0	-1.0	[]	379	
fourth_other	OTHER	|yem},|van},|ecu},|eth|,|bel},|nga},|gha},|mya|,|came,|neth,|name,|aust,|ger},|ned},|hkg|,|irq|,|sui},|civ},|tuni,|esp},|crc},|jpn},|fran,|ina},|swi},|usa},|nor},|png},|east,|mala,s foo,|por},|swe},|cze},|prk},|arg},|aut},|bra},|sin},|scot,|pan},|moro,}},|cro},|suda,px]],|mas},|tha},|can},|mli},|chil,|sene,|mada,|new,|qat},|eng},|para,|fin},|fra},|ecua,|yug},|peru,|uru},|bolo,|colo,|cana,px|bo,|s.p.,|col},|gre},|vie},|vene,|frg},|tur},|gua},	-1.0	-1.0	[]	390	
champions	OTHER	-1	-1.0	-1.0	[]	0	
city	OTHER	afric,cosaf,green,px]],er ca,uemoa,	-1.0	-1.0	[alt]	179	
slogan	OTHER	-1	-1.0	-1.0	[]	1	
matches	NUMBER	- nov,to,(expe,&ndas,july&,- jan,th -,march,– oct,– aug,july,– jan,–marc,- feb,-july,– sep,- dec,july–,(,- may,to th,/,,,-,–dece,- mar,–june,febru,th ju,may –,– apr,novem,to fe,to ja,–augu,– jun,june,–  ju,- aug,– jul,decem,-augu,-dece,–july,–,-sept,septe,- apr,june–,–octo,–sept,aprii,janua,– mar,april,–may,- oct,– dec,– nov,– may,– feb,may -,- jul,-octo,may–,- jun,to de,- sep,augus,in as,may &,−,octob,-may,sep,|,to ma,st se,	1.0	2015.0	[dates]	2916	
scorer	OTHER	-1	-1.0	-1.0	[]	2	
updated	OTHER	janua,may,june,decem,.,,,-,augus,march,octob,:,july,febru,novem,	-1.0	-1.0	[update]	421	
average_attendance	NUMBER	-1	13226.0	13226.0	[]	1	
preseason	OTHER	-1	-1.0	-1.0	[]	0	
champion	STRING	-1	-1.0	-1.0	[]	624	
federations	NUMBER	june,or,(plus,	1.0	44.0	[confederations]	973	
goalkeeper	OTHER	-1	-1.0	-1.0	[]	1	
American	STRING	-1	-1.0	-1.0	[]	1	
matches_footnote	NUMBER	-,	-1.0	-1.0	[]	1	
attendance	OTHER	+,	-1.0	-1.0	[]	276	
defending_champions	NUMBER	.ffc,	-1.0	-1.0	[]	1	
image	NUMBER	,w.png,ongc-,footb,_-_ba,uemoa,uefa,lg.jp,long,uae.j,asian,wwclo,march,champ,gulf,caf u,logo.,-milk,bid l,poste,_fifa,u-,worl,cosaf,- log,_matc,- nep,-,.svg,-uemo,).jpg,copa,_gree,summe,ccc l,-conc,w.jpg,wc,afc a,andor,_-_in,algar,.png,centr,arab,cecaf,px]],.jpg,_afc_,fifa,fifaw,–,emble,bidlo,afric,-bras,-fifa,m.jpg,.gif,east,_logo,s_cup,-copa,saff,-arab,conca,russi,canad,_medi,th hb,-cent,_sout,women,aff s,cpisr,- pos,offic,_-_sr,south,new l,-nile,-logo,world,-ceca,	-1.0	-1.0	[date, Image]	303	
venues	NUMBER	-août,(in,fifa,([[pr,]],	1.0	20.0	[venue]	902	
confederation	OTHER	-1	-1.0	-1.0	[]	1	
third	STRING	-1	-1.0	-1.0	[]	568	
size	NUMBER	conca,px,x,	90.0	200.0	[note]	594	
num_teams	NUMBER	(expe,,{{fb|,(grou,+'',(,fifa,]],(knoc,(fina,(elit,men's,(qual,''+,(main,(tota,(comp,(firs,(from,	2.0	205.0	[]	1712	
young_player	OTHER	-1	-1.0	-1.0	[]	1	
third_other	OTHER	|van},|ukra,footb,|mya|,|gha},|mex},|mya},|urug,|came,|name,|bfa},|blr},|hon},|ger},|ita},|ned},|slv},|ghan,|aus},|gdr},|esp},|cgo},|arge,|egy},|jpn},|fran,|ussr,}} [[,|norw,|urs},|cong,|csk},|isr},|beni,|por},|arg},|braz,|aut},|bra},|sin},|sol},|viet,|uae},}},|moro,|cro},|mas},|sout,|can},|tha},|chil,|indo,|gui},|eng},|para,|kor},|chi},socce,}}[[e,|fra},|kuw},|chn},|ecua,|ant|,|spai,|uru},|burk,|mar},|egyp,women,|yugo,|hun},|lao},|pol},|den},|vie},|gre},	-1.0	-1.0	[their_other]	461	
cities	NUMBER	]],	1.0	20.0	[]	906	
nextseason	NUMBER	afc c,afc b,&ndas,pan-p,mitro,brune,bsww,uefa,singa,]],caf b,afc p,cccf,caf c,afc u,afc w,milk,afc y,carib,hawai,firs,th na,jakar,cosaf,under,mundi,qatar,guadi,ussr,cfu c,gcc c,hondu,-,islan,copa,summe,austr,afc a,audi,centr,arab,tiger,cape,queen,casti,cecaf,all-a,king,–,india,saff,conca,eaff,turkm,green,sum u,cerh,viva,cpisr,kagam,south,|,asean,world,pacif,deser,swede,nehru,ofc w,ofc u,king',danub,beach,_afri,euro,long,campe,vff c,asian,grana,dynas,gulf,commo,th in,indon,amput,tna w,conme,u-,}} [[,'',walt,carol,medit,coupe,bids|,match,west,algar,europ,north,fifa,pekan,super,afric,merde,balti,sheik,east,la ma,toulo,aff w,emir,natio,rous,samsu,briti,aff s,women,ofc b,aff u,inter,pan a,torne,peace,ofc m,ofc n,	1969.0	2023.0	[]	1587	
second_other	OTHER	|scg},|gabo,footb,|denm,|nga},|czec,|mya|,|mex},perm|,|urug,|neth,|idn},|blr},|ngr},|hon},|ger},|ned},|ita},|irq},|gdr},|sui},|esp},|crc},|ukr},|arge,lever,|jpn},|côte,|fij},|fran,}} [[,|urs},|rwa},|usa},|jaka,. fc,|srb},),|cong,-,|por},|cze},|prk},|arg},|braz,|alge,|aut},|cmr},|sin},|bra},|togo,|bhr},|moro,}},|port,|mas},|sco},|tha},|can},|mli},|west,|sene,|qat},r ele,|uzb},|eng},|para,socce,|chi},|fra},|chn},|yug},|gree,|spai,|uru},|bolo,|irl},|colo,|burm,|egyp,women,|pol},|den},|col},|nige,|vie},|gre},|tah},. ffc,|gua},|tur},|par},	-1.0	-1.0	[]	935	
associations	NUMBER	-1	1.0	53.0	[]	132	
yearr	NUMBER	–,&ndas,(july,-pres,/,-,(firs,	3.0	2022.0	[year]	1749	
flagver	NUMBER	-1	1853.0	2004.0	[flagvar]	98	
top_scorers	NUMBER	– [[r,}}  [,) {{f,)|max,playe,) (fi,&nbsp,diffe,px]],)}},goal),)''',)'' -,px|ho,}} {{,: {{f,}} je,)|ali,}} [[,)|and,)|vik,[[wut,)  {{,'',- [[a,),)|pak,)   {,goal,goals,– [[ó,copa,– [[j,tied,	-1.0	-1.0	[top_scorer]	952	
goals_footnote	OTHER	-1	-1.0	-1.0	[]	0	
mascot	STRING	-1	-1.0	-1.0	[]	1	
tournament	STRING	-1	-1.0	-1.0	[Tournament]	4	
goalscorer2	OTHER	-1	-1.0	-1.0	[]	0	
finalists	NUMBER	(,	2.0	16.0	[]	94	
prevseason	NUMBER	afc c,&ndas,afc b,pan-p,mitro,brune,bsww,uefa,uemoa,singa,]],caf b,caf c,afc p,cccf,afc u,afc w,milk,afc y,far e,carib,firs,th na,jakar,rink,cosaf,under,mundi,qatar,guadi,gcc c,u.s.,cfu c,-,islan,copa,summe,austr,afc a,audi,arab,tiger,casti,cape,uncaf,queen,cecaf,zentr,all-a,king,–,india,wafu,saff,conca,eaff,turkm,green,sum u,ameri,viva,cpisr,kagam,south,|,asean,world,deser,nehru,ofc w,ofc u,king',beach,long,_afri,euro,vff c,campe,asian,grana,dynas,gulf,commo,th in,amput,tna w,u-,conme,}} [[,'',walt,carol,medit,milit,coupe,match,west,algar,europ,north,fifa,pekan,super,afric,merde,balti,sheik,east,la ma,toulo,aff w,emir,natio,rous,briti,women,aff s,ofc b,aff u,inter,pan a,torne,peace,ofc m,ofc n,	2010.0	2011.0	[]	1438	
Best_Goalkeeper	NUMBER	goals,	-1.0	-1.0	[]	1	
title	STRING	-1	-1.0	-1.0	[]	3	
winners_club	OTHER	-1	-1.0	-1.0	[]	0	
name	OTHER	fifa,	-1.0	-1.0	[]	4	
othertitles	OTHER	{{ar},,afc b,oro f,年亚足联u,“juve,uefa,גביע,rolle,''',afc w,''juv,nd fi,rink,de la,qatar,),cfu c,-,/sout,copa,европ,° tor,st fi,''' ،,年亚洲杯足,afl i,consu,femen,}} '',rd u-,''',,nd wa,年東亞運動,er ca,afcアジ,एएफसी,conca,{{nl,''{{p,تحت,''{{f,سنة,''{{e,''{{d,الإما,年亞足聯女,qadın,جام م,world,chan,campi,كأس ا,sea g,beach,campe,asian,vroue,champ,}} ([,{{now,korea,ans,prepa,kampe,fifaワ,youth,-em,년}} {,afc女子,ของผู,coupe,เอเชี,ecuad,suwon,afcon,europ,fifa,north,super,afric,asia,السعو,argen,futur,/rwan,-fußb,drago,women,st wa,ofc b,fifaク,schwe,aff u,sudam,urugu,ofc k,peace,سنة ل,da fi,	-1.0	-1.0	[other_titles]	549	
country8	STRING	-1	-1.0	-1.0	[country7]	1425	
winners	STRING	-1	-1.0	-1.0	[]	16	
champion_other	OTHER	|gabo,|scha,footb,|nga},|gha},|mya|,|mex},]],|mex|,|ngr},|hon},|ger},|ita},|ned},|hkg},st ti,|sui},|esp},|bass,|arge,|ukr},|egy},lever,|turk,|tch},|côte,|fran,}} [[,|urs},|ital,|germ,|usa},|rus},),th),|csk},|cong,|isr},|por},|cze},}}  (,|cent,|arg},|irn},|braz,|cmr},|bra},|moro,}},th) &,|engl,|mas},|tha},|west,–,|fin},|kor|,socce,|chi},|fra},|tun},|nzl},|spai,}} (s,|uru},|bolo,|irl},|colo,|cana,women,duisb,|pol},th ti,|col},|bur},|nige,|vie},|thai,. ffc,|tur},|gamb,	-1.0	-1.0	[]	1000	
imagesize	OTHER	-1	-1.0	-1.0	[]	0	
caption	OTHER	afc c,tourn,centr,nehru,cecaf,wwc o,barcl,uefa,sea g,fifa,king,asian,gulf,east,logo,conme,saff,conca,mundi,viva,cpisr,copa,offic,club,south,all-p,th ed,afc a,olymp,	-1.0	-1.0	[]	208	
winners_other	OTHER	-1	-1.0	-1.0	[]	1	
up	OTHER	-1	-1.0	-1.0	[]	0	
fourth	STRING	-1	-1.0	-1.0	[fifth]	441	
fived	STRING	-1	-1.0	-1.0	[]	1	
goals	NUMBER	or,+,	2.0	9642.0	[]	1268	

