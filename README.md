WikiEnt
=======
Rohit Girdhar
201001047

Methodology
-----------

I retrieve all the pages with InfoBoxes. (using wikixmlj)
Using a custom parser, read all the boxes into a data struct
`ent_type => (attr_name => (attr_info))`
`attr_info` contains all the information required for the attribute
like current seen max/min values, data type histogram for each attribute,
count of number of entities with a given attribute etc

HEURISTICS
----------

`entity_type` : Simply the text given after "infobox"
data types:
I use a probabilistic approach. I estimate the data type of each
attribute value (for any given entity), and make a histogram out of it. then 
find the most probable data type
- date : Used multiple heuristics
	 * text containing month name
	 * attr name with "date" in it
	 * integer value between 1200 - 2020
- Number : A string that can be parsed as a double
- List of strings : Strings separated by a ','
- Number with units : A double followed by non digits of length 1 to 5
- All the above (and others) implemented in DataTypeAnalyzer.java

Heuristics to find matching attribute:
- same data type
- edit distance between attribute names < 3

Average number of attributes per entity : 29
The number of entities per attribute are in the output. So the number of entity is the max{num of enitities with attrbute i}


-----
I used the following bash script to find the average number of attributes:
```bash
    #!/bin/bash
    l=0
    for file in `ls results_40gb`
    do
        ll=`wc -l results_40gb/$file`
        ll=`echo $ll | cut -f1 -d " "`
        l=`expr $l + $ll`
    done
    c=`ls results_40gb | wc -l`
    echo `expr $l / $c`
```

